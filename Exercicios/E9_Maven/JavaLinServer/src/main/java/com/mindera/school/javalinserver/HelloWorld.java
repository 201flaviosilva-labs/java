package com.mindera.school.javalinserver;

import io.javalin.Javalin;

public class HelloWorld {
    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7000);
        app.get("/Users/flaviosilva/Desktop/Mehul/JavaLinServer.html", ctx -> ctx.result("Hello World"));
    }
}