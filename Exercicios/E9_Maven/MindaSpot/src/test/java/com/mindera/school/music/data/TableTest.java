package com.mindera.school.music.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableTest {
    Table<Album> tabela;

    @BeforeEach
    void setUp() {
        tabela = new Table();
    }

    @Test
    void add() {
        Album album = new Album(1, "Test", 2019, 1, 1, 1);

        tabela.add(album);

        Assertions.assertEquals(album, tabela.backend.get(0));
    }

    @Test
    void testRemoveAfterAdding() {
        Album album = new Album(1, "Test", 2019, 1, 1, 1);

        tabela.add(album);
        tabela.remove(1);

        assertNull(tabela.find(1));
    }

    @Test
    void testRemoveAfterAdding2() {
        Album album = new Album(1, "Test", 2019, 1, 1, 1);
        Album album2 = new Album(2, "Test2", 20192, 12, 12, 12);

        tabela.add(album);
        tabela.add(album2);
        tabela.remove(1);

        assertEquals(1,tabela.backend.size());
        assertEquals(album2,tabela.backend.get(0));
    }

    @Test
    void testFindAfterAdding() {
        Album album = new Album(1, "Test", 2019, 1, 1, 1);

        tabela.add(album);

        Assertions.assertEquals(album,tabela.find(1));
    }

    @Test
    void testFindAllAfterAdding() {
        Album album = new Album(1, "Test", 2019, 1, 1, 1);
        Album album2 = new Album(2, "Test2", 20192, 12, 12, 12);
        Album album3 = new Album(3, "Test3", 201923, 123, 123, 123);

        tabela.add(album);
        tabela.add(album2);
        tabela.add(album3);


        Assertions.assertEquals(album,album);
    }
}