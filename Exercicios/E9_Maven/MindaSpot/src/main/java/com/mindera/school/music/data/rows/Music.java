package com.mindera.school.music.data.rows;


import com.mindera.school.music.data.Row;

public class Music implements Row {
    int music_id;
    String nome;
    int duration;
    int year;
    boolean explicit;
    String spootify_url;
    String youtube_url;
    int nr_search;
    int contry_id;
    int genre_id;
    int nr_plays;
    int nr_like;

    public Music() {
    }

    public Music(int music_id, String nome, int duration, int year, boolean explicit, String spootify_url, String youtube_url, int nr_search, int contry_id, int genre_id, int nr_plays, int nr_like) {
        this.music_id = music_id;
        this.nome = nome;
        this.duration = duration;
        this.year = year;
        this.explicit = explicit;
        this.spootify_url = spootify_url;
        this.youtube_url = youtube_url;
        this.nr_search = nr_search;
        this.contry_id = contry_id;
        this.genre_id = genre_id;
        this.nr_plays = nr_plays;
        this.nr_like = nr_like;
    }

    @Override
    public String toString() {
        return "Music{" +
                "music_id =" + music_id +
                ", nome ='" + nome + '\'' +
                ", duration =" + duration +
                ", year =" + year +
                ", explicit =" + explicit +
                ", spootify_url ='" + spootify_url + '\'' +
                ", youtube_url ='" + youtube_url + '\'' +
                ", nr_search =" + nr_search +
                ", contry_id =" + contry_id +
                ", genre_id =" + genre_id +
                ", nr_plays =" + nr_plays +
                ", nr_like =" + nr_like +
                '}';
    }

    public int getMusic_id() {
        return music_id;
    }

    public void setMusic_id(int music_id) {
        this.music_id = music_id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public boolean isExplicit() {
        return explicit;
    }

    public void setExplicit(boolean explicit) {
        this.explicit = explicit;
    }

    public String getSpootify_url() {
        return spootify_url;
    }

    public void setSpootify_url(String spootify_url) {
        this.spootify_url = spootify_url;
    }

    public String getYoutube_url() {
        return youtube_url;
    }

    public void setYoutube_url(String youtube_url) {
        this.youtube_url = youtube_url;
    }

    public int getNr_search() {
        return nr_search;
    }

    public void setNr_search(int nr_search) {
        this.nr_search = nr_search;
    }

    public int getContry_id() {
        return contry_id;
    }

    public void setContry_id(int contry_id) {
        this.contry_id = contry_id;
    }

    public int getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(int genre_id) {
        this.genre_id = genre_id;
    }

    public int getNr_plays() {
        return nr_plays;
    }

    public void setNr_plays(int nr_plays) {
        this.nr_plays = nr_plays;
    }

    public int getNr_like() {
        return nr_like;
    }

    public void setNr_like(int nr_like) {
        this.nr_like = nr_like;
    }

    @Override
    public int getid() {
        return 0;
    }
}
