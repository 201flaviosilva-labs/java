package com.mindera.school.music.databaseapi;

import com.mindera.school.music.data.rows.Country;
import com.mindera.school.music.data.tables.CountryTable;
import com.mindera.school.music.databaseapi.procedures.country.AddCountry;
import com.mindera.school.music.databaseapi.procedures.country.GetCountries;

import java.sql.*;

public class DBConnection {

    private static DBConnection MindDaSpot = null;

    private String HOST = "127.0.0.1:3306";
    private String DB_NAME = "Mind_Da_SPOT";
    private String USER = "flaviosilva";
    private String PASSWORD = "MinderaFixe";

    private Connection conn = null;
    private ResultSet rs = null;
    private Statement stmt = null;
    private CallableStatement cStmt = null;

    private DBConnection() {
    }

    public static DBConnection getInstance() {
        if (MindDaSpot == null) {
            MindDaSpot = new DBConnection();
        }
        return MindDaSpot;
    }

    public void openConnection() {
        try {
            System.out.printf("Connecting to database %s...%n", DB_NAME);

            conn = DriverManager.getConnection(String.format("jdbc:mysql://%s/%s", HOST, DB_NAME), USER, PASSWORD);

            System.out.println("Connected");
        } catch (SQLException e) {
            System.out.printf("SQLException: %s%n", e.getMessage());
            System.out.printf("SQLState: %s%n", e.getSQLState());
            System.out.printf("VendorError: %s%n", e.getErrorCode());
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            System.out.println("Closing connected...");
            conn.close();
            System.out.println("Connection closed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return this.conn;
    }

    public void addCountry(Country country) {
        AddCountry procedure = new AddCountry(country);
        procedure.execute();
    }

    public CountryTable getCountries() {
        GetCountries procedure = new GetCountries();
        return (CountryTable) procedure.execute();
    }
}
