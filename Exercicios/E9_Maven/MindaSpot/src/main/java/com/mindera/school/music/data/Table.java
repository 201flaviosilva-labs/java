package com.mindera.school.music.data;

import java.util.LinkedList;
import java.util.List;

public class Table<T extends Row> {
    List<T> backend;

    public Table() {
        this.backend = new LinkedList<>();
    }

    public void add(T x) {
        backend.add(x);
    }

    public void remove(int id) {
        for (int i = 0; i < backend.size(); i++) {
            if (id == backend.get(i).getid()) {
                backend.remove(i);
                break;
            }
        }
    }

    public T find(int id) {
        for (int i = 0; i < backend.size(); i++) {
            if (id == backend.get(i).getid()) {
                return backend.get(i);
            }
        }
        return null;
    }

    public List<T> findAll() {
        return backend;
    }
}
