package com.mindera.school.music.databaseapi.procedures.artist;

import com.mindera.school.music.data.rows.Artist;
import com.mindera.school.music.databaseapi.procedures.AbstractProcedure;

import java.sql.SQLException;

public class AddArtist  extends AbstractProcedure<Object> {
    private static final String PROCEDURE_NAME = "{call AddCountry(?)}";

    public AddArtist(Artist artist) {
        super(PROCEDURE_NAME);
        try {
            this.call.setString("artist_name", artist.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object handleResults() {
        return null;
    }

}
