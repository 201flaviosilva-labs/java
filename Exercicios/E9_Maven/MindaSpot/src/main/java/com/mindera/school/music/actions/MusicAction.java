package com.mindera.school.music.actions;

import com.mindera.school.music.ui.Action;
import com.mindera.school.music.ui.Menu;
import com.mindera.school.music.ui.Option;

public class MusicAction implements Action {
    Menu menu;

    public MusicAction() {
        menu = new Menu();
        menu.add(new Option("Add Music", new AddMusicAction()));
       // menu.add(new Option("Remove Music", new RemoveMusicAction()));
       // menu.add(new Option("Find Music", new FindMusicAction()));
       // menu.add(new Option("List All Musics", new FindAllMusicAction()));
       // menu.add(new Option("Voltar", new App()));
    }


    @Override
    public void execute() {
        menu.render();
    }
}
