package com.mindera.school.music.databaseapi.procedures.artist;

import com.mindera.school.music.data.rows.Artist;
import com.mindera.school.music.data.tables.ArtistTable;
import com.mindera.school.music.databaseapi.procedures.AbstractProcedure;

public class GetArtist extends AbstractProcedure<ArtistTable> {
    private static final String PROCEDURE_NAME = "{call GetArtist()}";

    public GetArtist() {
        super(PROCEDURE_NAME);
    }

    @Override
    protected ArtistTable handleResults() throws Exception {
        if (this.resultSet == null) throw new Exception("Invalid Result Set");

        ArtistTable table = new ArtistTable();
        while (this.resultSet.next()) {
            Artist artist = new Artist();

            artist.setId(this.resultSet.getInt("artist_id"));
            artist.setName(this.resultSet.getString("name"));
            artist.setCountry_id(this.resultSet.getInt("country_id"));
            artist.setDescription(this.resultSet.getString("description"));
            artist.setNr_searchs(this.resultSet.getInt("nr_searchs"));
            artist.setNr_followers(this.resultSet.getInt("nr_followers"));

            table.add(artist);
        }
        return table;
    }
}
