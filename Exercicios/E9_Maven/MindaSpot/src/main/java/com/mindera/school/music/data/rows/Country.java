package com.mindera.school.music.data.rows;

import com.mindera.school.music.data.Row;

public class Country implements Row {
    private int id;
    private String name;

    public Country() {
    }

    public void setId(int country_id) {
        this.id = country_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int getid() {
        return this.id;
    }
}
