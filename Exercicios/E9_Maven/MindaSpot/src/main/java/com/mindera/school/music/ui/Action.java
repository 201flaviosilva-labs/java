package com.mindera.school.music.ui;

public interface Action {
    void execute();
}
