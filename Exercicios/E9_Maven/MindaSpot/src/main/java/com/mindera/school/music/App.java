package com.mindera.school.music;

import com.mindera.school.music.actions.*;
import com.mindera.school.music.ui.Menu;
import com.mindera.school.music.ui.Option;

public class App {

    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.add(new Option("Add", new AddMusicAction()));
        menu.add(new Option("Find", new ListAllMusicAction()));
        menu.add(new Option("Sair", new NoAction()),true);
        menu.render();

    }
}
