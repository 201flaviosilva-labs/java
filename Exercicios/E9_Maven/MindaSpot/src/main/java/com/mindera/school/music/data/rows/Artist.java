package com.mindera.school.music.data.rows;


import com.mindera.school.music.data.Row;

public class Artist implements Row {
    private int id;
    private String name;
    private int country_id;
    private String description;
    private int nr_searchs;
    private int nr_followers;

    public Artist() {
    }

    public Artist(int id, String name, int country_id, String description, int nr_searchs, int nr_followers) {
        this.id = id;
        this.name = name;
        this.country_id = country_id;
        this.description = description;
        this.nr_searchs = nr_searchs;
        this.nr_followers = nr_followers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNr_searchs() {
        return nr_searchs;
    }

    public void setNr_searchs(int nr_searchs) {
        this.nr_searchs = nr_searchs;
    }

    public int getNr_followers() {
        return nr_followers;
    }

    public void setNr_followers(int nr_followers) {
        this.nr_followers = nr_followers;
    }

    @Override
    public int getid() {
        return this.id;
    }
}
