package com.mindera.school.music.databaseapi.procedures.country;

import com.mindera.school.music.data.rows.Country;
import com.mindera.school.music.databaseapi.procedures.AbstractProcedure;
import com.mindera.school.music.data.tables.CountryTable;



public class GetCountries extends AbstractProcedure {
    private static final String PROCEDURE_NAME = "{call GetCountries()}";

    public GetCountries() {
        super(PROCEDURE_NAME);
    }

    @Override
    protected CountryTable handleResults() throws Exception {
        if (this.resultSet == null) {
            throw new Exception("Invalid Result Set");
        }
        CountryTable table = new CountryTable();
        while (this.resultSet.next()) {
            Country country = new Country();
            country.setId(this.resultSet.getInt("country_id"));
            country.setName(this.resultSet.getString("name"));
            table.add(country);
        }
        return table;
    }
}
