package com.mindera.school.music.ui;

import java.util.LinkedList;
import java.util.Scanner;

public class Menu {
    private LinkedList<Option> options = new LinkedList<Option>();
    private Option exitOption;
    private Scanner keyboard = new Scanner(System.in);

    public void add(Option option) {
        options.add(option);
    }

    public void add(Option option, Boolean isExitOption) {
        if (isExitOption) {
            exitOption = option;
            options.add(exitOption);
        } else {
            options.add(option);
        }
    }

    public void render() {
        int op;
        do {
            for (int i = 0; i < options.size(); i++) {
                System.out.println(i + 1 + " - " + options.get(i).toString());
            }
            op = keyboard.nextInt();
            options.get(op - 1).execute();
        } while (options.get(op - 1) != exitOption);
    }
}
