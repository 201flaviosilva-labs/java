package com.mindera.school.music.databaseapi.procedures.music;

import com.mindera.school.music.data.rows.Music;
import com.mindera.school.music.databaseapi.procedures.AbstractProcedure;

import java.sql.SQLException;

public class AddMusic extends AbstractProcedure<Object> {
    private static final String PROCEDURE_NAME = "{call AddCountry(?)}";

    public AddMusic(Music music) {
        super(PROCEDURE_NAME);
        try {
            this.call.setString("music_name", music.getNome());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object handleResults() {
        return null;
    }
}
