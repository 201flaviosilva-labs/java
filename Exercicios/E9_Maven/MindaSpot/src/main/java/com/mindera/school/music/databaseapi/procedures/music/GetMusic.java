package com.mindera.school.music.databaseapi.procedures.music;

import com.mindera.school.music.data.rows.Music;
import com.mindera.school.music.data.tables.MusicTable;
import com.mindera.school.music.databaseapi.procedures.AbstractProcedure;

public class GetMusic extends AbstractProcedure<MusicTable> {
    private static final String PROCEDURE_NAME = "{call GetMusic()}";

    public GetMusic() {
        super(PROCEDURE_NAME);
    }

    @Override
    protected MusicTable handleResults() throws Exception {
        if (this.resultSet == null) throw new Exception("Invalid Result Set");

        MusicTable table = new MusicTable();
        while (this.resultSet.next()) {
            Music music = new Music();

            music.setMusic_id(this.resultSet.getInt("music_id"));
            music.setNome(this.resultSet.getString("name"));
            music.setDuration(this.resultSet.getInt("duration"));
            music.setYear(this.resultSet.getInt("year"));
            music.setExplicit(this.resultSet.getBoolean("Explicit"));
            music.setSpootify_url(this.resultSet.getString("spootify_url"));
            music.setYoutube_url(this.resultSet.getString("youtube_url"));
            music.setNr_search(this.resultSet.getInt("nr_search"));
            music.setContry_id(this.resultSet.getInt("contry_id"));
            music.setGenre_id(this.resultSet.getInt("genre_id"));
            music.setNr_plays(this.resultSet.getInt("nr_plays"));
            music.setNr_like(this.resultSet.getInt("nr_like"));

            table.add(music);
        }
        return table;
    }
}
