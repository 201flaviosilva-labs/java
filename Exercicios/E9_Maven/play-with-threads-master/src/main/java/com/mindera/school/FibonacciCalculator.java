package com.mindera.school;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to calculate the Fibonacci sequence
 * <p>
 * https://en.wikipedia.org/wiki/Fibonacci_number
 */
public class FibonacciCalculator {

    /**
     * Calculates n entries of the fibonacci sequence
     * <p>
     * In mathematics, the Fibonacci numbers, commonly denoted Fn, form a sequence, called the Fibonacci sequence,
     * such that each number is the sum of the two preceding ones, starting from 0 and 1
     * <p>
     * eg: 0,1,1,2,3,5,8,13,21,34,55,89,144
     */
    public String fibonacciSequence(final Integer n) {
        Integer num1 = 0;
        Integer num2 = 1;
        int count = 2;
        StringBuilder numbers = new StringBuilder(num1.toString() + "," + num2.toString());

        do {
            count++;
            num2 = num1 + num2;
            num1 = num2 - num1;
            numbers.append(",").append(num2.toString());
        } while (count < n);

        return numbers.toString();
    }

    /**
     * Calculates n entries of the fibonacci sequence for each entry in the list
     */
    public List<String> sequences(List<Integer> entries) {
        // TODO you should use threads for this;
        TreadClass cenas = new TreadClass();
        int JustNum = entries.size();

        cenas.run(entries.get(0));
        cenas.run(entries.get(1));
        cenas.run(entries.get(2));


        return List.of("");
    }
}