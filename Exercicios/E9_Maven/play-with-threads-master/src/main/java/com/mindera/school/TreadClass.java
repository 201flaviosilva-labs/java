package com.mindera.school;

public class TreadClass extends Thread {
    @Override
    public void run(int n) {
        Integer num1 = 0;
        Integer num2 = 1;
        int count = 2;
        StringBuilder numbers = new StringBuilder(num1.toString() + "," + num2.toString());

        do {
            count++;
            num2 = num1 + num2;
            num1 = num2 - num1;
            numbers.append(",").append(num2.toString());
        } while (count < n);

        // return numbers.toString();
    }
}