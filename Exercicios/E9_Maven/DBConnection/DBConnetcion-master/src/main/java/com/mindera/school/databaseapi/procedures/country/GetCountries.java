package com.mindera.school.databaseapi.procedures.country;

import com.mindera.school.databaseapi.entities.Country;
import com.mindera.school.databaseapi.procedures.AbstractProcedure;
import com.mindera.school.databaseapi.tables.CountryTable;

import java.util.ArrayList;


public class GetCountries extends AbstractProcedure {
    private static final String PROCEDURE_NAME = "{call GetCountries()}";

    public GetCountries() {
        super(PROCEDURE_NAME);
    }

    @Override
    protected CountryTable handleResults() throws Exception {
        ArrayList<Country> countryList = new ArrayList<>();
        if (this.resultSet == null) {
            throw new Exception("Invalid Result Set");
        }
        while (this.resultSet.next()) {
            Country country = new Country();
            country.setId(this.resultSet.getInt("country_id"));
            country.setName(this.resultSet.getString("name"));
            countryList.add(country);
        }
        CountryTable table = new CountryTable();
        table.setCountries(countryList);
        return table;
    }
}
