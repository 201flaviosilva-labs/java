package com.mindera.school.databaseapi.entities;

public class Country {
    private int id;
    private String name;

    public Country() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("%-4d%-25s", id, name);
    }
}
