package com.mindera.school.databaseapi.procedures;

import com.mindera.school.databaseapi.DBConnection;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbstractProcedure<T> {
    protected CallableStatement call;
    protected ResultSet resultSet;

    public AbstractProcedure(String name) {
        try {
            call = DBConnection.getInstance().getConnection().prepareCall(name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public T execute() {
        try {
            boolean hasResults = this.call.execute();

            if (hasResults) {
                this.resultSet = this.call.getResultSet();
                return this.handleResults();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    protected abstract T handleResults() throws Exception;

}
