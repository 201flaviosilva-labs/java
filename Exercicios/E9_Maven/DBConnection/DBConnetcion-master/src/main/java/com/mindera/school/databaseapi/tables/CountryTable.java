package com.mindera.school.databaseapi.tables;

import com.mindera.school.databaseapi.entities.Country;

import java.util.ArrayList;

public class CountryTable {
    private ArrayList<Country> countries;

    public CountryTable() {
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }

    public void setCountries(ArrayList<Country> countries) {
        this.countries = countries;
    }

    public void printTable() {
        System.out.println("Country list:");
        System.out.printf("ID%2sName%21s\n", "", "");
        countries.stream().forEach(c -> System.out.println(c.toString()));
    }
}
