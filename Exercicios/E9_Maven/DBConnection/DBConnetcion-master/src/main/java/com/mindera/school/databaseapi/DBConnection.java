package com.mindera.school.databaseapi;

import com.mindera.school.databaseapi.entities.Country;
import com.mindera.school.databaseapi.procedures.country.AddCountry;
import com.mindera.school.databaseapi.procedures.country.GetCountries;
import com.mindera.school.databaseapi.tables.CountryTable;
import java.sql.*;

public class DBConnection {

    private static DBConnection MindDaSpot = null;

    private String HOST = "127.0.0.1:3306";
    private String DB_NAME = "Mind_Da_SPOT";
    private String USER = "flaviosilva";
    private String PASSWORD = "MinderaFixe";

    private Connection conn = null;
    private ResultSet rs = null;
    private Statement stmt = null;
    private CallableStatement cStmt = null;

    private DBConnection() {
    }

    public static DBConnection getInstance() {
        if (MindDaSpot == null) {
            MindDaSpot = new DBConnection();
        }
        return MindDaSpot;
    }

    public void openConnection() {
        try {
            System.out.printf("Connecting to database %s...%n", DB_NAME);

            conn = DriverManager.getConnection(String.format("jdbc:mysql://%s/%s", HOST, DB_NAME), USER, PASSWORD);

            System.out.println("Connected");
        } catch (SQLException e) {
            System.out.printf("SQLException: %s%n", e.getMessage());
            System.out.printf("SQLState: %s%n", e.getSQLState());
            System.out.printf("VendorError: %s%n", e.getErrorCode());
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            System.out.println("Closing connected...");
            conn.close();
            System.out.println("Connection closed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return this.conn;
    }

    public void getCountriesSP() {
        try {
            stmt = conn.createStatement();

            boolean hadResults = stmt.execute("SELECT * FROM country ORDER By country_id ASC");

            if (hadResults) {
                rs = stmt.getResultSet();
                System.out.println("Results: " + rs.getFetchSize());
                System.out.println("Country list:");
                System.out.printf("ID%2sName%21s\n", "", "");
                while (rs.next()) {
                    Country country = new Country();
                    System.out.println(country);
                }
            }

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            // it is a good idea to release
            // resources in a finally{} block
            // in reverse-order of their creation
            // if they are no-longer needed

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore

                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
    }

    public void getCountrySP() {
        try {
            cStmt = conn.prepareCall("{call GetCountries()}");

            boolean hadResults = cStmt.execute();

            if (hadResults) {
                ResultSet rs = cStmt.getResultSet();

                System.out.println("Country list:");
                System.out.printf("ID%2sName%21s\n", "", "");
                while (rs.next()) {
                    Country country = new Country();
                    country.setId(rs.getInt("country_id"));
                    country.setName(rs.getString("name"));
                    System.out.println(country);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addCountry(Country country) {
        AddCountry procedure = new AddCountry(country);
        procedure.execute();
    }

    public CountryTable getCountries() {
        GetCountries procedure = new GetCountries();
        return (CountryTable) procedure.execute();
    }
}
