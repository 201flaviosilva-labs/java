package com.mindera.school;

import com.mindera.school.databaseapi.DBConnection;
import com.mindera.school.databaseapi.entities.Country;
import com.mindera.school.databaseapi.tables.CountryTable;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        DBConnection db = DBConnection.getInstance();
        db.openConnection();

        Country newCountry = new Country();
        newCountry.setName("Germany");

        db.addCountry(newCountry);

        CountryTable countryTable = db.getCountries();
        countryTable.printTable();



        db.closeConnection();
    }
}
