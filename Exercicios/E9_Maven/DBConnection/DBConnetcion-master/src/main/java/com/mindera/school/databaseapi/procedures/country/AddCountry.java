package com.mindera.school.databaseapi.procedures.country;

import com.mindera.school.databaseapi.entities.Country;
import com.mindera.school.databaseapi.procedures.AbstractProcedure;

import java.sql.SQLException;

public class AddCountry extends AbstractProcedure {
    private static final String PROCEDURE_NAME = "{call AddCountry(?)}";

    public AddCountry(Country country) {
        super(PROCEDURE_NAME);
        try {
            this.call.setString("country_name", country.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Object handleResults() {
        return null;
    }
}

