public class BaralhoDeCartas {
	public static void main(String[] args) {

		System.out.println("-----------------------------");
		System.out.println("------ Mostrar Cartas -------");
		System.out.println("-----------------------------");
		System.out.println("------ Olha o Baralho -------");
		System.out.println("-----------------------------");

		String[] naipes = {"Clubs ", "Diamonds ", "Hearts ", "Spades "};
		String[] numeros = {"2", "3", "4", "5", "6", "7", "8", "9", "10","Jack", "Queen", "King", "Ace"};
		int numeroCartas = naipes.length * numeros.length;
		String[] baralho = new String[numeroCartas];	
		int count3 = 0;

		for (int count1 = 0; count1 < naipes.length; count1++) {
			for (int count2 = 0; count2 < numeros.length; count2++) {
				baralho[count3]=naipes[count1] + numeros[count2];
				System.out.println(count3+" "+baralho[count3]);
				++count3;
			}//end for
		}//end for
		System.out.println("-----------------------------");
		System.out.println("---- Vou baralhar, olha: ----");
		System.out.println("-----------------------------");

		for (int count1 = 0; count1 < numeroCartas; count1++) {
			int cartaRandom=(int) (Math.random()*numeroCartas);
			String cartaTrocada=baralho[count1];
			baralho[count1]=baralho[cartaRandom];
			baralho[cartaRandom]=cartaTrocada;
			System.out.println(count1+" "+baralho[count1]);
		}//end for
		System.out.println("-----------------------------");
		System.out.println("---------- Baralhei ---------");
		System.out.println("-----------------------------");
	}//end method
}//end Class