import java.util.*;

public class FuncionReplace{
	
	public static void main(String[] args) {
	
		char[] text = "AC".toCharArray();
		char[] replace = "ZX".toCharArray();
		char[] find = "CD".toCharArray();

		int found = find(text, find);
		System.out.println(found);
		//System.out.println(Arrays.toString(replace(text, found, replace)));

	}//end of main

	public static int find(char[] text, char[] find) {//verifica por cada elemento de find se é igual a algum dos elemntos do text, se sim, verifica  retribui o indice do mesmo

		for (int i = 0; i < text.length - find.length + 1; i++) {

			boolean found = true;
			for (int j = 0; j < find.length; j++ ) {

				if(text[j + i] != find[j]){

					found = false;
					break;

				}//end of if
			}//end of for

			if (found){
				return i;
			}
		}//end of for

		return -1;
	}//end of method replace

	/*public static char[] replace(char[] text, int found, char[] replace) {

		for(int i = 0; i < text.length; i++){
			
			if(text[i]==text[found]){
					text[i]=replace[i];
			}
		}

		return text;
	}*/
}//end of class