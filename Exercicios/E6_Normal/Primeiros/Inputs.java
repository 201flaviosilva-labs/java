import java.util.Scanner;//Importa a biblioteca que permite o utilizador escrever
public class Inputs{
	public static void main(String[] args) {
		Scanner myVar = new Scanner(System.in);//Recebe algo que o utilizador quiser
		System.out.print("Escreve alguma coisa: ");
		String recebido=myVar.nextLine(); //Manda o valor recebido para uma variavel tipo String
        System.out.println("Disses-te: "+recebido); // outra opção -> myVar.nextLine() 
        
        /*
        Read a byte - nextByte()
		Read a short - nextShort()
		Read an int - nextInt()
		Read a long - nextLong()
		Read a float - nextFloat()
		Read a double - nextDouble()
		Read a boolean - nextBoolean()
		Read a complete line "String" - nextLine()
		Read a word "char" - next()
        */
	}
}