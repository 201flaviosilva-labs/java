 public class RandomMaiorMenor{
	public static void main(String[] args) {
		double num1=Math.random(); //double num1=(Math.random()*6)+1 -> cria um valor de 6 a 1 (com casas decimais)
		double num2=Math.random(); //int num2=(int) (Math.random()*6)+1 -> cria um valor de 6 a 1 (sem casas decimais)
		double num3=Math.random();
		double num4=Math.random();
		double num5=Math.random();

		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);
		System.out.println(num5);

		double minimo=Math.min(num1, Math.min(num2, Math.min(num3, Math.min(num4, num5))));	
		double maximo=Math.max(num1, Math.max(num2,Math.max (num3, Math.max(num4, num5))));	

		System.out.println("Número Minimo: "+minimo);
		System.out.println("Número Máximo: "+maximo);

		System.out.println("Médio: "+(num1+num2+num3+num4+num5)/5);

	}
}