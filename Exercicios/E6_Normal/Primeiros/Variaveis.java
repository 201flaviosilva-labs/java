public class Variaveis{
	public static void main(String[] args) {

		String palavras="Flávio"; //Varia que permite guardar conjunto de letras
		String ultimoNome="Silva"; //Outra variavel de tipo String
		String nomeCompleto=palavras+" "+ultimoNome; //Junção de Variaveis
		int num1=18; //Varia que permite guardar número insteiros
		double num2=31.10; //Varia que permite guardar números com virgulas
		char letra='S'; //Varia que permite guardar uma letra
		boolean simNao=true;

		System.out.println("Nome: "+palavras);
		System.out.println("Último nome: "+ultimoNome);
		System.out.println("Idade: "+num1);
		System.out.println("Data de Namoro: "+num2);
		System.out.println("Letra do ultimo nome: "+letra);
		System.out.println("Juntar Nomes: "+palavras+ultimoNome);
		System.out.println("Juntar Nomes 2: "+nomeCompleto);
		System.out.println("Boolean"+simNao);
	}
}