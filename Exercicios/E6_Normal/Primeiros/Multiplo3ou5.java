import java.util.Scanner; // importar a biblioteca Scanner 

public class Multiplo3ou5{
	public static void main(String[] args) {
		Scanner receber = new Scanner(System.in);

		System.out.println("Descobrir a multiplicade dos número");

		System.out.print("Insere um número: ");
                int num1 = receber.nextInt();

                System.out.println("--------------");

                //Contas-------
                int mul3=num1%3;
                int mul5=num1%5;

                if (mul3==0 && mul5!=0) {
                	System.out.println("Fizz");
                }
                else if (mul5==0 && mul3!=0) {
                	System.out.println("Buzz");
                }
                else if (mul5==0 && mul3==0) {
                	System.out.println("FizzBuzz");
                }
                else{
                	System.out.println("Error!!");
                }
        }
}