public class OperadoresIncrementacao{
	public static void main(String[] args) {
		int num1=0;
		System.out.println(num1);
		System.out.println(++num1);//Adiciona antes de mostrar a variavel
		System.out.println(num1++);//Adiciona depois de mostar a variavel
		System.out.println(num1);

		int num2=10;
		System.out.println(num2);
		System.out.println(--num2);//Subtrai antes de mostrar a variavel
		System.out.println(num2--);//Subtrai depois de mostar a variavel
		System.out.println(num2);

		int num3=40;
		System.out.println(num3);
		num3+=20;
		System.out.println("+20= "+num3);

		int num4=40;
		System.out.println(num4);
		num4-=20;
		System.out.println("-20= "+num4);
	}
}