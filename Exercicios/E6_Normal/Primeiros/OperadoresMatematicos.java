public class OperadoresMatematicos{
	public static void main(String[] args) {
		int num1=10;
		int num2=20;

		int adicao=num1+num2;
		int subtracao=num2-num1;
		int multiplicacao=num1*num2;
		int divisao=num2/num1;
		int resto1=num2%num1; //Dá o resto da divisao de dois números
		int resto2=5%3;//o resto de 5/3=2

		System.out.println("10+20="+adicao);
		System.out.println("20-10="+subtracao);
		System.out.println("10*20="+multiplicacao);
		System.out.println("20/10="+divisao);
		System.out.println("O resto da divisão de 20/10="+resto1);
		System.out.println("O resto da divisão de 5/3="+resto2);

	}
}