public class Triangulo{
  public static void main(String[] args) {
    int altura = Integer.parseInt(args[0]);
    int espacos = altura;

    if (altura>=0){
      for (int count1=0; count1<altura ; count1++ ) {
        for (int count3=espacos; count3>=count1 ; count3-- ) {
          System.out.print(" ");
        }
        for (int count2=0; count2<=count1 ; count2++ ) {
          System.out.print("@ ");
        }
        System.out.println("");
      }
    }
  
    else{
      System.out.println("Error");
    }
  }
}

/*public class _9_1Triangulo {
   public static void main(String[] args) {
       int altura = Integer.parseInt(args[0]);
       int espacos = altura;
       if (altura > 1){
           for (int count1 = 0; count1 < espacos; count1++) {
               for (int count3 = espaços; c >= count1; count3--)
                   System.out.print(" ");
               for (int count2 = 0; ccount2*2 <= count1*4; count3++)
                   System.out.print("@");
               System.out.println();
           }
       }
       else {
        System.out.println("A altura tem de ser maior que 1!");
       }
   }
}*/

