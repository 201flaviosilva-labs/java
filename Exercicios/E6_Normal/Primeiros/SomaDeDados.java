import java.util.Random;

public class SomaDeDados {

    public static void main(String [] args) {
        Random rand = new Random(); 
        int num1=rand.nextInt(6)+1;//Gera um número de 1 a 6
        int num2=rand.nextInt(6)+1;//Gera um número de 1 a 6
        int resp=num1+num2;
        System.out.println("Número 1: "+num1);
        System.out.println("Número 2: "+num2); 
        System.out.println("Resultado: "+resp);
    }
    }
