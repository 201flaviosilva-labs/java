package com.mindera.school.Funcoes.Funcoes;

import java.util.*;
public class FuncoesMaior{
	public static void main(String[] args) {
		int [] vetor = new int [] {2,3,4,3,2};
		int [] vetor2 = new int [] {8,309,7,24,5};
		int [] vetor3 = new int [] {1,2,3,4,5};
		System.out.println("Maior: " + numeroMaior(vetor));
		System.out.println("Média: " + numeroMedia(vetor));
		System.out.println("Namorados: " + somaNamorados(vetor));
		vetor2 = OrdenarMenorMaior(vetor2);
		System.out.println("Ordenar: " + Arrays.toString(vetor2));
		vetor3 = Rotacoes(vetor3,1);
		System.out.println("Rodar: " + Arrays.toString(vetor3));
	}//end method main

	public static int numeroMaior(int[] numeros){
		int numero_maior = Integer.MIN_VALUE;

		for (int count=0; count < numeros.length; count++) {
			/*
			if (numeros[count] > numero_maior){
				numero_maior = numeros[count];
			}//end if
			*/
			numero_maior = Math.max(numeros[count], numero_maior);
		}//end for

		return numero_maior;
	}//end method numeroMaior


	public static int numeroMedia(int[] numeros){
		int media_numeros=0;

		for (int count=0; count < numeros.length; count++) {
			media_numeros += numeros[count];
		}//end for

		media_numeros = media_numeros / numeros.length;
		return  media_numeros;
	}//end method numeroMaior

	/*
	public static int numeroMedia(int[] numeros){
		int media_numeros = 0;

		for (int count=0; count < numeros.length; count++) {
			media_numeros += numeros[count];
		}//end for

		media_numeros = media_numeros / numeros.length;
		return  media_numeros;
	}//end method numeroMaior
	*/

	public static int somaNamorados(int[] numeros){
		int namorados=0;

		for (int count=0; count < numeros.length; count++) {
			if (numeros[count] % 2 == 0){
			namorados += numeros[count];
			}// end if
		}//end for

		return  namorados;
	}//end method numeroMaior

	public static int[] OrdenarMenorMaior(int[] numerosInvertidos){
		int trocar=0;

		for (int count1 = 0; count1 < numerosInvertidos.length; count1++) { //length
			for (int count2 = count1; count2 < numerosInvertidos.length; count2++) { //length
				if (numerosInvertidos[count1] > numerosInvertidos[count2]) { 
					trocar = numerosInvertidos[count1];
					numerosInvertidos[count1] = numerosInvertidos[count2];
					numerosInvertidos[count2] = trocar;
				}//end if
			}//end for
		}//end for
		return  numerosInvertidos;
	}//end method numeroMaior

	public static int[] Rotacoes(int[] numerosRodar, int nRotacoes){
		int rodar = 0;
		int ultimoNumeroArray = numerosRodar.length - 1;

		for (int count2 = 0; count2 < nRotacoes; count2++) {

			/*rodar = numerosRodar[ultimoNumeroArray];
			numerosRodar[ultimoNumeroArray] = numerosRodar[0];
			numerosRodar[0] = rodar;*/

			//rodar = numerosRodar[0];
			//numerosRodar[0] = numerosRodar[ultimoNumeroArray];
			//numerosRodar[ultimoNumeroArray] = rodar;

			for (int count1 = 0; count1 < ultimoNumeroArray; count1++) {

				// rodar = numerosRodar[ultimoNumeroArray];
				// numerosRodar[ultimoNumeroArray] = numerosRodar[count1];
				// numerosRodar[count1] = rodar;

				rodar = numerosRodar[count1];
				numerosRodar[count1] = numerosRodar[ultimoNumeroArray];
				numerosRodar[ultimoNumeroArray] = rodar;
			}//end for

		}//end for

		return  numerosRodar;
	}//end method numeroMaior

}//end class
