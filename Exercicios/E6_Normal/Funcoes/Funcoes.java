package com.mindera.school.Funcoes.Funcoes;

public class Funcoes{
	public static void main(String[] args) {
		System.out.println("Começou o programa");
		System.out.println("Vamos fazer exercicios");
		System.out.println("");

		System.out.println("Um facil: ");
		Holla();
		System.out.println("");

		System.out.println("Agora uma soma");
		System.out.println(soma(2,3));
		System.out.println("");

		System.out.println("Agora outro exercicio de soma mas mais complicado");
		int sdf =cenas(new int[] {2, 10, 3});
		System.out.println(sdf);
		System.out.println("");

		System.out.println("Agora outro exercicio de string to int: ");
		StringToInt(new String[] {"2", "4", "3"});
		/*for (int count1 =0 ;count1 < StringToInt.length;count1++ ) {
			System.out.println(StringToInt[count1]);
		}*/
		System.out.println("");

		System.out.println("Ainda não fui embora por isso mais um: ");
		System.out.println(soma2(StringToInt(args)));
		System.out.println("E o programa acabou :'( !!");
	}//end method main

	static void Holla() {
		System.out.println("Holla Mundo");
		System.out.println(3+5);
	}//end method Holla

	static int soma(int num1, int num2) {
		return num1 + num2;
	}//end method soma


	static int cenas(int[] numeros) {
		int soma = 0;
		for (int count = 0; count < numeros.length; count++) {
			soma += numeros[count];
		}//end do 
		System.out.println(soma);
		return soma;
	}//end method cenas

	static int[] StringToInt(String[] letras) {
		int[] numeros = new int [letras.length];
		for (int count = 0; count < letras.length; count++) {
			numeros[count] =Integer.parseInt(letras[count]);
		}//end do for
		return numeros;
	}//end method StringToInt

	static int soma2(int[] numerosPsomar) {
		int resultadoDaSoma = 0;
		for (int count = 0; count < numerosPsomar.length; count++) {
			resultadoDaSoma += numerosPsomar[count];
		}//end do for
		return resultadoDaSoma;
	}//end method soma2
}//end class
