package com.mindera.school.Funcoes.Funcoes;

import java.util.*;
public class FindReplaceExercise {
    public static void main(String[] args) {
        testCase1();
        testCase2();
        testCase3();
    }

    public static int findAndReplace(char[] text, char[] find, char[] replace) {
        
    }

    public static void testCase1() {
        System.out.println("testCase1");

        char[] text = "João Batista Coelho, ou melhor, Slow J é um músico português que nasceu e viveu na cidade de Setúbal até aos 8 anos...".toCharArray();
        char[] find = "Batista".toCharArray();
        char[] replace = "Batxsta".toCharArray();
        
        char[] result = findAndReplace(text, find, replace);
        
        assertEquals("João Batxsta Coelho, ou melhor, Slow J é um músico português que nasceu e viveu na cidade de Setúbal até aos 8 anos...".toCharArray(),
            result, "Should replace all occurrence of 'Batista' by 'Batxsta'");
    }

    public static void testCase2() {
        System.out.println("testCase2");

        char[] text = "João Batista Coelho, ou melhor, Slow J é um músico português que nasceu e viveu na cidade de Setúbal até aos 8 anos...".toCharArray();
        char[] find = "músico inglês".toCharArray();
        char[] replace = "músico qwerty".toCharArray();

        char[] result = findAndReplace(text, find, replace);

        assertEquals("João Batista Coelho, ou melhor, Slow J é um músico português que nasceu e viveu na cidade de Setúbal até aos 8 anos...".toCharArray(),
            result, "Should not replace anything");
    }

    public static void testCase3() {
        System.out.println("testCase3");

        char[] text = "João Batista Coelho, ou melhor, Slow J é um músico português que nasceu e viveu na cidade de Setúbal até aos 8 anos...".toCharArray();
        char[] find = "lho".toCharArray();
        char[] replace = "___".toCharArray();
        
        char[] result = findAndReplace(text, find, replace);
        
        assertEquals("João Batxsta Coe__, ou me___r, Slow J é um músico português que nasceu e viveu na cidade de Setúbal até aos 8 anos...".toCharArray(),
            result, "Should replace all occurrence of 'lho' by '___'");
    }

    public static void assertEquals(char[] expected, char[] actual, String message) {
        if (Arrays.equals(expected, actual)) {
            System.out.println("OK");
        } else {
            System.err.println("Failed: " + message);
        }
    }
}