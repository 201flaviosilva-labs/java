package com.mindera.school.Funcoes.Funcoes;

public class AlterarLetraFuncao{
	public static void main(String[] args) {
		char[] text = "AC".toCharArray();
		char[] replace = "ZX".toCharArray();
		char[] find = "CD".toCharArray();

		int found = find(text, find);
		System.out.println(found);
	}//end method main

	public static int find(char [] text, char [] find){

			for (int count1 = 0; count1 < text.length; count1++) {
            boolean found = true;
                for (int count2 = 0; count2 < find.length; count2++) {
                    if (text[count1 + count2] != find[count2]){
                        found = false;
                        break;
                    }//end if
                }//end for

                if (found){
                    return count1;
                }
            }//end for
        return -1;
	}//end method alterar
}//end class