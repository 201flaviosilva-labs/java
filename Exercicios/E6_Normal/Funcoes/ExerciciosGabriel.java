package com.mindera.school.Funcoes.Funcoes;

import java.util.*; //repete 1 vez
public class ExerciciosGabriel{ //repete 1 vez
	public static void main(String[] args) { //repete 1 vez
		int [] vetor2 = new int [] {8,309,7,24,5}; //repete 1 vez
		int [] vetor3 = new int [] {1,2,3,4,5}; //repete 1 vez
		if (vetor2.length == 0) { ///repete 1 vez
			System.out.println("Error 1"); //repete 1 vez
		}//end if //repete 1 vez
		else{ //repete 1 vez
			vetor2 = OrdenarMenorMaior(vetor2); // repete 1 vez
			System.out.println("Ordenar: " + Arrays.toString(vetor2)); //repete 1 vez
		}//end else //repete 1 vez

		if (vetor3.length == 0) { //repete 1 vez
			System.out.println("Error 2"); //repete 1 vez
		}//end if
		else{ //repete 1 vez
			vetor3 = Rotacoes(vetor3,4);//repete 1 vez
			System.out.println("Rodar: " + Arrays.toString(vetor3));//repete 1 vez
		}//end else //repete 1 vez
	}//end method main

	public static int[] OrdenarMenorMaior(int[] numerosInvertidos){
			int trocar = 0; //repete 1 vezes

			for (int count1 = 0; count1 < numerosInvertidos.length; count1++) { //repete x vezes
				for (int count2 = count1; count2 < numerosInvertidos.length; count2++) { //repete x * x vezes
					if (numerosInvertidos[count1] > numerosInvertidos[count2]) { //repete x * x vezes
						trocar = numerosInvertidos[count1]; //repete x * x vezes
						numerosInvertidos[count1] = numerosInvertidos[count2];//repete x * x vezes
						numerosInvertidos[count2] = trocar; //repete x * x vezes
					}//end if //repete x * x vezes
				}//end for //repete x * x vezes
			}//end for //repete x vezes	
		return  numerosInvertidos; //repete 1 vez
	}//end method numeroMaior //repete 1 vez


	public static int[] Rotacoes(int[] numerosRodar, int nRotacoes){
			int rodar = 0; //repete 1 vez
			int ultimoNumeroArray = numerosRodar.length - 1; //repete 1 vez

			for (int count2 = 0; count2 < nRotacoes; count2++) { //repete n vezes
				for (int count1 = 0; count1 < ultimoNumeroArray; count1++) { //repete x * x vezes
					rodar = numerosRodar[ultimoNumeroArray]; //repete x * x vezes
					numerosRodar[ultimoNumeroArray] = numerosRodar[count1]; //repete x * x vezes
					numerosRodar[count1] = rodar; //repete x * x vezes
				}//end for //repete x * x vezes
			}//end for //repete n vezes
		return  numerosRodar; //repete 1 vez
	}//end method numeroMaior //repete 1 vez
}//end class //repete 1 vez

// A complexidade algoritmica do metodo OrdenarMenorMaior é numerosInvertidos.length ao quadrado segundo a minha conclusão

// o Nome da complexidade é -> Selection Sort

// https://www.treinaweb.com.br/blog/conheca-os-principais-algoritmos-de-ordenacao/

// A complexidade algoritima segundo o site é -> C(n) = O(n²)
