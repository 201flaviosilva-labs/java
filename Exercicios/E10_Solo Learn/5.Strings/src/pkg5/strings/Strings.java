
package pkg5.strings;

public class Strings {

    public static void main(String[] args) {
        String firstName, lastName;
        firstName = "David";
        lastName = "Williams";

        System.out.println("O meu nome é: " + firstName +" "+lastName);
        
        // Saida: O meu nome é: David Williams
    }
    
}
