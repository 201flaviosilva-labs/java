
package pkg12.forloops.ciclo.pkgfor;

public class ForLoopsCicloFor {

    public static void main(String[] args) {
        //Criação de número inteiro
        //repete Aque que "x" seja maior ou igual a 5
     for(int x = 0; x <5;) {
         x++;
  System.out.println(x);
  //Fim ciclo
}

/* Outputs
1
2
3
4
5
*/   
    }
    
}
