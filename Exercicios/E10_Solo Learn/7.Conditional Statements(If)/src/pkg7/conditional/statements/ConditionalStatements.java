package pkg7.conditional.statements;

public class ConditionalStatements {

    public static void main(String[] args) {
        //Introdução de Variavel
int idade = 45;

//condição, se ele tiver idade menor que 10 então é muito novo
if (idade < 10) {
   System.out.println("Demasiado novo");
   //se não, é bem vindo
} else { 
   System.out.println("Bem vindo!");
}
//Saida "Bem vindo"
    }
    
}
