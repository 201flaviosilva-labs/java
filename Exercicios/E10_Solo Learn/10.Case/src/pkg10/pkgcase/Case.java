
package pkg10.pkgcase;

public class Case {

    public static void main(String[] args) {
        //Idrodução do dia
        int dia = 6;

        //se caso o dia foi 1 então...
        switch(dia) {
        case 1:
             System.out.println("Segunda-feira");
             //Para o "se"
         break;
         //se caso o dia foi 2 então...
        case 2:
             System.out.println("Terça-feira");
             //Para o "se"
        break;
        //se caso o dia foi 3 então...
        case 3:
             System.out.println("Quarta-feira");
             //Para o "se"
        break;
        // Se não for nenhum dos de cima, então...
        default:
    System.out.println("Não sei que dia é");
}
}
// Saida "Quarta-feira"
    }

