
package pkg14.arrays;

public class Arrays {

    public static void main(String[] args) {
        //NO JAVA OS ARRAYS COMEÇÃO COM  "0"
        //Formas de declarar Vetores
        //int[] num2;
        //New Int 5 -> Número de vetores criados;
        int [] num2 = new int[5];
        num2[0]=2;
        num2[1]=67;
        num2[2]=5;
        num2[3]=32;
        num2[4]=200;
        for(int x = 0; x <5;) {
  System.out.println("O vétor "+ x + " é " + num2[x]);
  x++;
        }
        
        //Vetores com letras
        String[ ] Nomes = { "A", "B", "C", "D"};
        //A=0, B=1, C=2, D=3;
        System.out.println(Nomes[2]);
        // Saida "C"
    }
    
}
