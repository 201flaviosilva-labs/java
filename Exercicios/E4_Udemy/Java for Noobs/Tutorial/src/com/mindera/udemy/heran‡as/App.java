package com.mindera.udemy.heranças;

import com.mindera.udemy.classs.Animal;

public class App {

	public static void main(String[] args) {
		Animal animal = new Animal("Pig", 25);
		
		Dog dog = new Dog("Sabrina", 1);
		System.out.println(dog.getName());
		System.out.println(dog.getAge());
		
		Cat cat = new Cat("Formiga", 7);
		System.out.println(cat.getName());
		System.out.println(cat.getAge());
		
		//Animal animal2 = cat;
		System.out.println(cat.speak());
		System.out.println(dog.speak());
		
		System.out.println(animal);
		System.out.println(cat.toString());
		System.out.println(dog.toString());
		
		Animal animal3 = new Animal("Kalvin", 12);
		
		Animal [] animals = new Animal[3];
		animals[0]=animal;
		animals[1]=animal;
		animals[2]=animal3;
		
		for (int i = 0; i < animals.length; i++) {
			System.out.println(animals[i].toString());
		}
	}

}
