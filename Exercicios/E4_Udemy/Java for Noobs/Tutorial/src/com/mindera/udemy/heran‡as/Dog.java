package com.mindera.udemy.heranças;

public class Dog extends Animal {
	public Dog(String name, int age) {
		super(name, age);
	}
	public String speak() {
		return "Au!";
	}
	
	public String toString() {
		return "Name: " + getName() + " Age: " + getAge();
	}
}
