package com.mindera.udemy.classs;

public class Index {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Animal animal = new Animal("Flávio", 18);
		System.out.println(animal.getName());
		System.out.println(animal.getAge());
		
		animal.setName("Silva");
		animal.setAge(20);
		
		System.out.println(animal.getName());
		System.out.println(animal.getAge());
	}

}
