package com.mindera.udemy.heranças;

public class Cat extends Animal {

	public Cat(String name, int age) {
		super(name, age);
	}
	
	public String speak() {
		return "Miau";
	}
}
