package com.mindera.udemy.abstract2;

public class Dog extends Animal {
	public Dog(String name, int age) {
		super(name, age);
	}
	public String speak() {
		return "Au!";
	}
	
	public String toString() {
		return "Name: " + getName() + " Age: " + getAge();
	}
}
