package com.mindera.udemy.abstract2;

public class Cat extends Animal {

	public Cat(String name, int age) {
		super(name, age);
	}
	
	public String speak() {
		return "Miau";
	}
}
