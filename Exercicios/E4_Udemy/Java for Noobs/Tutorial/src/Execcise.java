
public class Execcise {

	public static void main(String[] args) {
		int[] x = { 1, 5, 7, 15, 24, 12, 14, 13, 4, 2, 23 };
		System.out.println("Even(par) " + sumEven(x));
		System.out.println("Odd(Impar) " + sumOdd(x));
		System.out.println("Maximo " + maxNum(x));
		System.out.println("Minimo " + minNum(x));
		System.out.println("Avage " + avage(x));
	}

	public static int sumEven(int[] x) {
		int sum = 0;
		for (int i = 0; i < x.length; i++) {
			if ((x[i] % 2) == 0) {
				sum += x[i];
			}
		}
		return sum;
	}

	public static int sumOdd(int[] x) {
		int sum = 0;
		for (int i = 0; i < x.length; i++) {
			if (x[i] % 2 == 1) {
				sum += x[i];
			}
		}
		return sum;
	}

	public static int maxNum(int[] x) {
		int max = 0;
		for (int i = 0; i < x.length; i++) {
			if (max < x[i]) {
				max = x[i];
			}
		}
		return max;
	}

	public static int minNum(int[] x) {
		int min = x[0]; // maxNum(x);
		for (int i = 0; i < x.length; i++) {
			if (min > x[i]) {
				min = x[i];
			}
		}
		return min;
	}

	public static double avage(int[] x) {
		int sum = maxNum(x);
		for (int i = 0; i < x.length; i++) {
			sum += x[i];
		}
		return sum / x.length;
	}
}
