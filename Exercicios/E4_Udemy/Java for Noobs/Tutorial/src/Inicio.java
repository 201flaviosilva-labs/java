import java.util.Scanner;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1;
		num1 = 5;
		System.out.println(num1);

		double num2;
		num2 = 4.6;
		System.out.println(num2);

		boolean trueorfalse;
		trueorfalse = true;
		System.out.println(trueorfalse);

		char crarater;
		crarater = 'C';
		System.out.println(crarater);

		String firstName = "Flávio";
		String lastName = "Silva";
		String fullName = firstName + " " + lastName;
		System.out.println("Ful name: " + firstName + lastName);
		System.out.println(firstName + " " + lastName);
		System.out.println(firstName + 123);
		System.out.println(fullName);

		int numInt1 = 7;
		int soma = num1 + numInt1;
		int subtraction = num1 - numInt1;
		System.out.println(num1 + numInt1);
		System.out.println(subtraction);
		System.out.println(num1 / numInt1);
		System.out.println(num1 * numInt1);
		System.out.println(num1 % numInt1);

		// relational operatores
		int a1 = 5;
		int a2 = 10;
		int a3 = 0;
		System.out.println(a1 < a2);
		System.out.println(a1 > a2);
		System.out.println(a1 == a2);
		System.out.println(a1 <= a2);
		System.out.println(a1 >= a2);
		System.out.println(a1 != a2);

		// if statment
		if (a1 < a2 && a3 == 2) {
			System.out.println("a1 < a2 && a3 = 0");
		} else if (a1 == a2 || a3 == 0) {
			System.out.println("a1 = a2 || a3 == 3");
		} else {
			System.out.println("a1 > a2");
		}

		String animal = "Cat";
		switch (animal) {
		case "Cat":
			System.out.println("Cat");
			break;
		case "Dog":
			System.out.println("Dog");
			break;
		default:
			System.out.println("Nada");
		}
		int sum = add(2, 1);
		System.out.println(sum);
		String juntoString = addString("Flávio ", "Silva");
		System.out.println(juntoString);

		prePost(5);
		loopFor("HeyFor");
		loopWhile("HeyWhile");
		loopDoWhile("HeyDoWhile");
		looopFofFor(4, 5);
		
		//Arrays
		int [] aIntArray = {8,9,7,6,5,4,3};
		int [] bIntArray = new int [10];
		for (int i = 0; i < aIntArray.length; i++) {
			System.out.println(aIntArray[i]);
			bIntArray[i]=i;
			System.out.println(bIntArray[i]);
		}
		System.out.println();
		
		//Arrays 2d -> Matrises
		int row = 2;
		int coll = 5;
		int [][] bIntMatriz =new int [row][coll];
		int [][] aIntMatriz ={{0,1,2,3},{4,5,6},{7,8,9}};
		for (int i = 0; i < aIntMatriz.length; i++) {
			for (int j = 0; j < aIntMatriz[i].length; j++) {
				System.out.print(aIntMatriz[i][j] + " ");
			}
			System.out.println();
		}
		
		Scanner rc = new Scanner(System.in);
		System.out.println("Write your name: ");
		String firstNameString = rc.nextLine();
		System.out.println("Hey " + firstNameString);
		
		/*
        Read a byte - nextByte()
		Read a short - nextShort()
		Read an int - nextInt()
		Read a long - nextLong()
		Read a float - nextFloat()
		Read a double - nextDouble()
		Read a boolean - nextBoolean()
		Read a complete line "String" - nextLine()
		Read a word "char" - next()
        */
	}

	public static void looopFofFor(int a, int b) {
		for (int i = 0; i < a; i++) {
			for (int j = 0; j < b; j++) {
				System.out.print("+ ");
			}
			System.out.println();
		}
	}

	public static int add(int a, int b) {
		return a + b;
	}

	public static String addString(String a, String b) {
		return a + b;
	}

	private static void prePost(int x) {
		System.out.println(x);
		System.out.println(x++);
		System.out.println(x);
		System.out.println(++x);
	}

	private static void loopFor(String fraseString) {
		for (int i = 1; i <= 10; i++) {
			System.out.println(i + " " + fraseString);
		}
	}

	private static void loopWhile(String fraseString) {
		int i = 0;
		while (i < 10) {
			System.out.println(i + " " + fraseString);
			i++;
		}
	}

	private static void loopDoWhile(String fraseString) {
		int i = 0;
		do {
			System.out.println(i + " " + fraseString);
			i++;
		} while (i < 0);
	}
}
/**
 * dfgvcdsv dvdfvc dfvfdc
 * 
 */
//Sdfghjk