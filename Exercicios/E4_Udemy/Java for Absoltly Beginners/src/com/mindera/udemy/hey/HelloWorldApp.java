package com.mindera.udemy.hey;

public class HelloWorldApp {
    public static void main(String[] args) {
        System.out.println("Hey");
        String name = "Flávio";
        System.out.print("My name is" + name);
        System.out.println("Hey " + name);
        System.out.println();


        //data Types
        byte by1 = 23;
        short s1 = 245;
        int i1 = 20000;
        long l1 = 2000000000;
        double d1 = 20.5;
        float f1 = 15.5f;
        boolean bo1 = true;
        char c1 = 'A';

        System.out.println("byte " + by1);
        System.out.println("short " + s1);
        System.out.println("int " + i1);
        System.out.println("long " + l1);
        System.out.println("double " + d1);
        System.out.println("float " + f1);
        System.out.println("boolean " + bo1);
        System.out.println("char " + c1);

        //if and else and else if
        System.out.println();
        System.out.println("If, else, else if");
        if (by1 == 24) {
            System.out.println("by1 = 24");
        } else if (by1 < 24) {
            System.out.println("by1 < 24");
        } else if (by1 > 24) {
            System.out.println("by1 > 24");
        } else {
            System.out.println("Error");
        }
        //AND and OR
        System.out.println();
        System.out.println("AND and OR");
        int grade = 98; //média
        int pass = 0;
        if (grade >= 0 && grade < 20) {
            System.out.println("Nota final = 1");
            pass = 1;
        } else if (grade >= 20 && grade < 50) { // && -> e
            System.out.println("Nota final = 2");
            pass = 2;
        } else if (grade >= 50 && grade < 70) {
            System.out.println("Nota final = 3");
            pass = 3;
        } else if (grade >= 70 && grade < 90) {
            System.out.println("Nota final = 4");
            pass = 4;
        } else if (grade >= 90 && grade <= 100) {
            System.out.println("Nota final = 5");
            pass = 5;
        } else if (grade < 0 || grade > 100) {
            System.out.println("Error of grade");
            pass = 0;
        } else {
            System.out.println("Error");
        }
        if (pass == 1 || pass == 2) { // || -> ou
            System.out.println("You dont have pass");
        } else if (pass == 3 || pass == 4 || pass == 5) {
            System.out.println("You have pass, congratulations");
        } else {
            System.out.println("Error");
        }

        //Switch and Case
        System.out.println();
        System.out.println("Switch and Case");
        String gradeString = "A+";
        switch (gradeString) {
            case "A+":
                System.out.println("You are a great student");
                break;
            case "A":
                System.out.println("You are a great student");
                break;
            case "A-":
                System.out.println("You are a great student");
                break;
            case "B+":
            case "B":
            case "B-":
                System.out.println("You are a great student");
                break;
            default:
                System.out.println("YOU FAIL THIS CITY");
                break;
        }
        //Arrays
        String[] namesArray = new String[]{"A", "B", "C", "D", "E", "F"};
        int [] ageArray = new int[]{ 25, 45, 67, 89};
    }
}
