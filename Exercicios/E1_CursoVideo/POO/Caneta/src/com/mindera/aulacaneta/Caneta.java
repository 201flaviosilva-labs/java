package com.mindera.aulacaneta;

public class Caneta {
    public String modelo;
    private String cor;
    private double tamanho;
    private boolean tampada;

    public Caneta(String m, String c , int t){ //O metudo constrotor tem o mesmo nome que a class
        this.modelo = m;
        this.cor = c;
        this.tamanho = t;
        this.tampar();
    }

    public String getModelo() {
        return this.modelo;
    }

    public void setModelo(String modl) {
        this.modelo = modl;
    }

    public double getTamanho() {
        return this.tamanho;
    }

    public void setTamanho(double tam) {
        this.tamanho = tam;
    }

    public void tampar(){
        this.tampada=true;
    }

    public  void destampar(){
        this.tampada=false;
    }

    public void estado() {
        System.out.println("Estado da Caneta:");
        System.out.println(" Modelo: " + this.getModelo());
        System.out.println(" Cor: " + this.cor);
        System.out.println(" Tamanho: " + this.getTamanho());
        System.out.println(" Tampa: " + this.tampada);
    }
}
