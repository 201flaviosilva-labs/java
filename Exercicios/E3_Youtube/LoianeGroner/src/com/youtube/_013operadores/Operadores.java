package com.youtube._013operadores;

public class Operadores {
    public static void main(String[] args) {
        //Aritemetics
        int num1 = 10, num2 = 20, num3 = 30, num4 = 15;
        System.out.println("num1 = 10 \n num2 = 20 \n num3 = 30 \n num4 = 15");

        int sum = num1 + num3;
        System.out.println("Sum num1 + num3 = " + sum);

        int sub = num3 + num4;
        System.out.println("Subtration num3 - num4 = " + sub);

        int mult = num4 * num2;
        System.out.println("Multiplication num4 * num2 = " + mult);

        int div = num3 / num1;
        System.out.println("Division num1 / num3 = " + div);

        int mod = num2 % num4;
        System.out.println("Modulo num1 % num3 = " + mod + " Show the rest of division");

        System.out.println("Incremente and decremenet!");
        System.out.println("Num1 = " + num1);
        System.out.println("++num1 = " + ++num1 + " sum + 1");
        System.out.println("num1++ = " + num1++ + " sum + 1 after print");
        System.out.println("Num1 = " + num1);
        System.out.println("--num1 = " + --num1 + " sub - 1");
        System.out.println("num1-- = " + num1-- + " sub - 1 after print");
        System.out.println("Num1 = " + num1);
        num2 += 1;
        System.out.println("num2 += 1" + num2 + "it's the same of -> num2 = num2 + 1;");
        num3 += num1;
        System.out.println("num3 + = num1; -> " + num3 + "num3 = num3 + num1");

        //Strings
        System.out.println();
        String word1 = "String con", word2 = "catenada";
        System.out.println("word1 = \"String con\" \n word2 = \"catenada\"");
        String uniao1 = word1 + word2;
        System.out.println("word1+word2 = " + uniao1);
        String uniao2 = word1 + " " + word2;
        System.out.println("word1 + \" \" + word2 = " + uniao2);

        //Operadores relacionais
        System.out.println();
        System.out.println("Relactions operatores");
        System.out.println("1 == 2: " + (1 == 2)); //false
        System.out.println("2 == 2: " + (2 == 2)); //true
        System.out.println("1 < 2: " + (1 < 2)); // true
        System.out.println("1 > 2: " + (1 > 2)); // false
        System.out.println("1 <= 2: " + (1 <= 2)); // true
        System.out.println("1 >= 2: " + (1 >= 2)); // false
        System.out.println("1 <= 2: " + (1 <= 1)); // true
        System.out.println("1 >= 2: " + (1 >= 1)); // true
        System.out.println("1 != 2: " + (1 != 1)); //false
        System.out.println("1 != 2: " + (1 != 2)); //true

        //Operadores Logicos
        System.out.println();
        System.out.println("Relactions lógicos");
        boolean t = true;
        boolean f = false;
        System.out.println("t = true \n f = false");
        System.out.println("t && t: " + (t && t)); // true
        System.out.println("t && t: " + (t && f)); // false
        System.out.println("t && t: " + (f && f)); // false

        System.out.println("t || t: " + (t || t)); // true
        System.out.println("t || f: " + (t || f)); // true
        System.out.println("f || f: " + (f || f)); // false

        System.out.println("t ^ t: " + (t ^ t)); // false
        System.out.println("t ^ f: " + (t ^ f)); // true
        System.out.println("f ^ f: " + (f ^ f)); // false

        System.out.println("t ! t: " + !(t &&t)); // false
        System.out.println("t ! f: " + !(t &&f)); // true
        System.out.println("f ! f: " + !(false &&false)); // false


    }
}
