package com.youtube._012classscanner;

import java.util.Scanner;

public class ClassScanner {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Hey, welcome to de Java!");
        System.out.println("Look Java is amazing, so....");
        System.out.print("What is your name? ");
        String name = keyboard.nextLine();
        System.out.println("You name in Java code is: " + name + " it's the same!");

        /*
        Read a byte - nextByte()
		Read a short - nextShort()
		Read an int - nextInt()
		Read a long - nextLong()
		Read a float - nextFloat()
		Read a double - nextDouble()
		Read a boolean - nextBoolean()
		Read a complete line "String" - nextLine()
		Read a word "char" - next()
        */

    }
}
