package com.youtube._011tiposprimitivos;

public class TiposPrimitivos {
    public static void main(String[] args) {
        System.out.println("Integer tipes");
        byte by = 20;
        short s = 20;
        int i = 20;
        long l = 20;
        System.out.println("byte -> " + by);
        System.out.println("short -> " + s);
        System.out.println("int -> " + i);
        System.out.println("long -> " + l);
        System.out.println("Using decimal table");
        int iDecVal = 26;
        int iHexVal = 0x1a; //26
        int iOctVal = 032; //26
        int iBinVal = 0b11010; //26
        System.out.println("int decimal -> " + iDecVal);
        System.out.println("int Hex -> " + iHexVal);
        System.out.println("int Oct -> " + iOctVal);
        System.out.println("int Binary -> " + iBinVal);
        System.out.println("Usiang \" _ \" to separate numbers");
        int i_number_numebr = 26_30_50;
        System.out.println("Number_Number -> " + i_number_numebr);

        //
        System.out.println();
        System.out.println("Decimal tipes");
        float f = 20.4f;
        double d = 20.5;
        System.out.println("float -> " + f);
        System.out.println("double -> " + d);

        //
        System.out.println();
        System.out.println("char tipes");
        char c1 = 'C';
        char c2 = 111;
        System.out.println("char -> " + c1);
        System.out.println("char2 (table ASCII) -> " + c2);

        //
        System.out.println();
        System.out.println("bollean tipes");
        boolean bo1 = true;
        boolean bo2 = false;
        System.out.println("bo1 -> " + bo1);
        System.out.println("bo2 -> " + bo2);
    }
}
