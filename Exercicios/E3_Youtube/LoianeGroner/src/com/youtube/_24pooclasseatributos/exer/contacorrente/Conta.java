package com.youtube._24pooclasseatributos.exer.contacorrente;

public class Conta {
    private int numero;
    private int saldo;
    private boolean expecial;
    private int limite;

    public Conta(int numero, int saldo, boolean expecial, int limite) {
        this.numero = numero;
        this.saldo = saldo;
        this.expecial = expecial;
        this.limite = limite;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public boolean isExpecial() {
        return expecial;
    }

    public void setExpecial(boolean expecial) {
        this.expecial = expecial;
    }

    public int getLimite() {
        return limite;
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }

    @Override
    public String toString() {
        return "Conta{" +
                "numero=" + numero +
                ", saldo=" + saldo +
                ", expecial=" + expecial +
                ", limite=" + limite +
                '}';
    }
}
