package com.youtube._24pooclasseatributos.exer.contacorrente;

public class App {
    public static Conta conta = new Conta(1, 500, false, 10_000);

    public static void main(String[] args) {
        sacarDinheiro(700);
        sacarDinheiro(300);
        sacarDinheiro(200);
        sacarDinheiro(50);

        System.out.println("Dinehiro na conta: " + conta.getSaldo());

        depositarDinheiro(500);
        depositarDinheiro(600);
        depositarDinheiro(1_400);
        depositarDinheiro(10_000);
        depositarDinheiro(9_000);
        depositarDinheiro(7_500);
        depositarDinheiro(1);

        sacarDinheiro(5_000);

        depositarDinheiro(1_000, true);
        depositarDinheiro(1_500, false);
        depositarDinheiro(500, true);


    }

    public static void sacarDinheiro(int sacar) {
        System.out.println();
        System.out.println("Sacar " + sacar + "€");
        if (sacar <= conta.getSaldo()) {
            conta.setSaldo(conta.getSaldo() - sacar);
            System.out.println("Saque bem sucedido");
        } else {
            System.out.println("Error falta de dinheiro");
        }
        System.out.println("Dinehiro na conta: " + conta.getSaldo() + "€");
        System.out.println();
    }

    public static void depositarDinheiro(int deposito) {
        System.out.println();
        System.out.println("Depositar " + deposito + "€");
        if (deposito + conta.getSaldo() > conta.getLimite()) {
            System.out.println("Erro conta não consegue guardar tanto dinheiro");
        } else {
            conta.setSaldo(conta.getSaldo() + deposito);
            System.out.println("Deposito bem sucedido");
        }
        System.out.println("Saldo conta " + conta.getSaldo() + "€");
        System.out.println("");
    }

    public static void depositarDinheiro(int deposito, boolean cheque) {
        System.out.println();
        System.out.println("Depositar " + deposito + "€");
        if (deposito + conta.getSaldo() > conta.getLimite() || ((conta.isExpecial() == false) && cheque==true)) {
            System.out.println("Erro conta não consegue guardar tanto dinheiro ou tipo de tranferencia indisponivel");
        } else {
            conta.setSaldo(conta.getSaldo() + deposito);
            System.out.println("Deposito bem sucedido");
        }
        System.out.println("Saldo conta " + conta.getSaldo() + "€");
        System.out.println("");
    }
}
