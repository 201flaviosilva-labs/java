package com.youtube._24pooclasseatributos.exer.aluno;

import java.util.LinkedList;

public class App {
    public static void main(String[] args) {
        LinkedList<String> disciplinas = new LinkedList<>();
        disciplinas.add("Alquimia");
        disciplinas.add("Java");
        disciplinas.add("JavaScript");

        LinkedList<Integer> notas = new LinkedList<>();
        notas.add(4);
        notas.add(10);
        notas.add(16);
        Aluno alunos = new Aluno("Zé", "Java", disciplinas, notas);
    }
}
