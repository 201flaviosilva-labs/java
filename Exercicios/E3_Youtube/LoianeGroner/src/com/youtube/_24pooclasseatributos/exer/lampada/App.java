package com.youtube._24pooclasseatributos.exer.lampada;

import com.youtube._24pooclasseatributos.exer.lampada.Lampada;

public class App {
    public static void main(String[] args) {

        Lampada lampada = new Lampada(true);
        System.out.println("A lampada está: " + lampada.getStado());
        lampada.setStado(false);
        System.out.println("A lampada está: " + lampada.getStado());
        lampada.setStado(true);
        System.out.println("A lampada está: " + lampada.getStado());
    }
}
