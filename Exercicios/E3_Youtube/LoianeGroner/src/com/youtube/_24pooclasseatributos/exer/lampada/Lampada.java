package com.youtube._24pooclasseatributos.exer.lampada;

public class Lampada {
    private boolean stado; // true = ligado; false = desligado

    public Lampada(boolean stado) {
        this.stado = stado;
    }

    public String getStado() {
        return (stado ? "Ligada" : "Desligada");
    }

    public void setStado(boolean stado) {
        this.stado = stado;
    }
}
