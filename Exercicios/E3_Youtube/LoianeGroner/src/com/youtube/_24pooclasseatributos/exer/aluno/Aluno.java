package com.youtube._24pooclasseatributos.exer.aluno;

import java.util.LinkedList;

public class Aluno {
    private String nome;
    private String curso;
    private LinkedList<String> disciplinas;
    private LinkedList<Integer> notas;

    public Aluno(String nome, String curso, LinkedList<String> disciplinas, LinkedList<Integer> notas) {
        this.nome = nome;
        this.curso = curso;
        this.disciplinas = disciplinas;
        this.notas = notas;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public LinkedList<String> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(LinkedList<String> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public LinkedList<Integer> getNotas() {
        return notas;
    }

    public void setNotas(LinkedList<Integer> notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return "Aluno{" +
                "nome='" + nome + '\'' +
                ", curso='" + curso + '\'' +
                ", disciplinas=" + disciplinas +
                ", notas=" + notas +
                '}';
    }
}
