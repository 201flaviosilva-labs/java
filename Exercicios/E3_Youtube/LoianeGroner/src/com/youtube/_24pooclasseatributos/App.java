package com.youtube._24pooclasseatributos;

public class App {
    public static void main(String[] args) {
        Car van = new Car();
        Car corrida = new Car();
        Car desportivo = new Car();
        Car classico = new Car();

        System.out.println();
        System.out.println("Van");
        van.marca = "Fiat";
        van.modelo = "ModeloC";
        van.nPassageiros = 10;
        van.capCombustivel = 100;
        van.consumoCombus = 5;
        System.out.println("Marca " + van.getMarca());
        System.out.println("Modelo " + van.getModelo());
        System.out.println("N. Passageiros " + van.getnPassageiros());
        System.out.println("Cap. Combustivel " + van.getCapCombustivel());
        System.out.println("Consumo Combustivel " + van.getConsumoCombus());

        System.out.println();
        System.out.println("Corrida");
        corrida.marca = "CarsFixe";
        corrida.modelo = "Racing";
        corrida.nPassageiros = 50;
        corrida.capCombustivel = 100000000;
        corrida.consumoCombus = 50;
        System.out.println("Marca " + corrida.getMarca());
        System.out.println("Modelo " + corrida.getModelo());
        System.out.println("N. Passageiros " + corrida.getnPassageiros());
        System.out.println("Cap. Combustivel " + corrida.getCapCombustivel());
        System.out.println("Consumo Combustivel " + corrida.getConsumoCombus());

        System.out.println();
        System.out.println("Desportivo");
        desportivo.marca = "CarsFixe";
        desportivo.modelo = "Desporto";
        desportivo.nPassageiros = 2;
        desportivo.capCombustivel = 35;
        desportivo.consumoCombus = 10;
        System.out.println("Marca " + desportivo.getMarca());
        System.out.println("Modelo " + desportivo.getModelo());
        System.out.println("N. Passageiros " + desportivo.getnPassageiros());
        System.out.println("Cap. Combustivel " + desportivo.getCapCombustivel());
        System.out.println("Consumo Combustivel " + desportivo.getConsumoCombus());

        System.out.println();
        System.out.println("Classico");
        classico.marca = "Volkswagen";
        classico.modelo = "Beetle";
        classico.nPassageiros = 5;
        classico.capCombustivel = 500;
        classico.consumoCombus = 0.8;
        System.out.println("Marca " + classico.getMarca());
        System.out.println("Modelo " + classico.getModelo());
        System.out.println("N. Passageiros " + classico.getnPassageiros());
        System.out.println("Cap. Combustivel " + classico.getCapCombustivel());
        System.out.println("Consumo Combustivel " + classico.getConsumoCombus());

        System.out.println();
        System.out.println("Van: " + van.toString());
        van.autonomia();
        System.out.println("Corrida: " + corrida.toString());
        corrida.autonomia();
        System.out.println("Desportivo: " + desportivo.toString());
        desportivo.autonomia();
        System.out.println("Classico: " + classico.toString());
        classico.autonomia();
    }
}
