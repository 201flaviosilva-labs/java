package com.youtube._24pooclasseatributos;

public class Car {
    String marca;
    String modelo;
    int nPassageiros;
    double capCombustivel;
    double consumoCombus;

    void autonomia(){
        System.out.println("Atonomia = Capacidade de Combustivel * Consumo de Combustivel = " + this.capCombustivel * this.consumoCombus);

    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getnPassageiros() {
        return nPassageiros;
    }

    public void setnPassageiros(int nPassageiros) {
        this.nPassageiros = nPassageiros;
    }

    public double getCapCombustivel() {
        return capCombustivel;
    }

    public void setCapCombustivel(double capCombustivel) {
        this.capCombustivel = capCombustivel;
    }

    public double getConsumoCombus() {
        return consumoCombus;
    }

    public void setConsumoCombus(double consumoCombus) {
        this.consumoCombus = consumoCombus;
    }

    @Override
    public String toString() {
        return "Car{" +
                "marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                ", nPassageiros=" + nPassageiros +
                ", capCombustivel=" + capCombustivel +
                ", consumoCombus=" + consumoCombus +
                '}';
    }
}

