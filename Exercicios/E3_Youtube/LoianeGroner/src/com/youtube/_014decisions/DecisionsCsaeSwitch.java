package com.youtube._014decisions;

public class DecisionsCsaeSwitch {
    public static void main(String[] args) {
        int week = 2;
        switch (week){
            case 1:
                System.out.println("Munday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thusday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                System.out.println("Error, impossible day");
                break;
        }
    }
}
