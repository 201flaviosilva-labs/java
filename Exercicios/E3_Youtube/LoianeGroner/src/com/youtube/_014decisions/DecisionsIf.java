package com.youtube._014decisions;

public class DecisionsIf {
    public static void main(String[] args) {
        int idade = 18;

        if (idade < 0) {
            System.out.println("Error");
        } else if (idade >= 0 && idade <= 5) {
            System.out.println("Baybe");
        } else if (idade >= 6 && idade <= 12) {
            System.out.println("Child");
        } else if (idade >= 13 && idade <= 17) {
            System.out.println("Young");
        } else if (idade >= 18 && idade <= 65) {
            System.out.println("Adult");
        } else {
            System.out.println("Old");
        }
    }
}
