package com.youtube._19arrays;

public class ArraysClass {
    public static void main(String[] args) {
       int [] num1 = {1,2,3,4,5,6};
       int [] num2 = new int[5];

       num2[0] = 20;
       num2[1] = 45;
       num2[2] = 12;
       num2[3] = 3435;
       num2[4] = 678;

        for (int i = 0; i < num2.length; i++) {
            System.out.println(num2[i]);
        }
        System.out.println();
        for (int i: num1) {
            System.out.println(i);

        }

    }
}
