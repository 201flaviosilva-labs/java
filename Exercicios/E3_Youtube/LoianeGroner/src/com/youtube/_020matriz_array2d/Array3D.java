package com.youtube._020matriz_array2d;

public class Array3D {
    public static void main(String[] args) {
        int[][][] num = {{{1,2,3},{4,5,6},{7,8,9},{10,11,12}},{{13,14,15},{16,17,18},{19,20,21},{22,23,24}},{{25,26,27},{28,29,30},{30,31,32},{33,34,35}},{{36,37,38},{39,40,41},{42,43,44},{45,46,47}}};
        for (int i = 0; i < num.length; i++) {
            for (int j = 0; j < num[i].length; j++) {
                for (int k = 0; k < num[i][j].length; k++) {
                    System.out.print(num[i][j][k] + " ");
                }
                System.out.println("");
            }
            System.out.println();
        }
    }
}
