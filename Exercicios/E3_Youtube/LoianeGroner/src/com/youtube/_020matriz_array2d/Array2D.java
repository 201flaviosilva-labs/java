package com.youtube._020matriz_array2d;

public class Array2D {
    public static void main(String[] args) {
        int [][] num1 = new int[10][4]; // 10 -> Colunas; 4 -> Colunas
        int[][] notasAlunos = {
            {1,2,3,4}, // 0
            {5,6,7,8}, // 1
            {9,0,1,2}, // 2
            {3,4,5,6}, // 3
            {7,8,9,0}}; // 4
        for (int i = 0; i < notasAlunos.length; i++) {
            for (int j = 0; j < notasAlunos[i].length; j++) {
                System.out.print(notasAlunos[i][j] + " ");
            }
            System.out.println();
        }
    }
}
