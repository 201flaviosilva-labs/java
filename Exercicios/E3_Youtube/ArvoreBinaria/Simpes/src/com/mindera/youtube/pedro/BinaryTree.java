package com.mindera.youtube.pedro;

public class BinaryTree<T> {
    private Node<T> root;

    private void add(T value) {
        if (this.root == null) {
            this.root = new Node<>(value);
        } else {
            Node tmp = this.root;
            while (tmp.left != null && tmp.right != null) {
                tmp = tmp.left;
            }
            if (tmp.left == null) {
                tmp.left = new Node<>(value);
            } else {
                tmp.right = new Node<>(value);
            }
        }
    }

    public static void main(String[] args) {
        BinaryTree t = new BinaryTree();
        t.add(10);
        t.add(12);
        t.add(11);
        t.add(14);
        t.add(9);
        t.add(20);
        t.add(22);
        t.add(19);
    }
}

