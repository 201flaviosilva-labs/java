package com.mindera.youtube.binarytree;

public class App {
    public static void main(String[] args) {
        Node node = new Node(15, new Node(10, null, null), new Node(50, new Node(20, null, null), null));

        System.out.println("Imprimir: ");
        Node.imprimirPreOrdem(node);

        System.out.println();
        System.out.println(node.toString());
    }
}
