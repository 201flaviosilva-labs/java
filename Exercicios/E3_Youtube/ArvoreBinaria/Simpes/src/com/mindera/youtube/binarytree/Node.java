package com.mindera.youtube.binarytree;

public class Node {
    int value;
    private Node left;
    private Node right;

    public Node(int value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public static void imprimirPreOrdem(Node n){
        System.out.println(n.value);
        if (n.left != null) {
            imprimirPreOrdem(n.left);
        }
        if (n.right != null) {
            imprimirPreOrdem(n.right);
        }
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
