package com.mindera.princeton.primeiro_capitulo.primeiro_subcapitulo;

public class Variaveis {
    public static void main(String[] args) {
        int a, b, c;
        a = 1234;
        b = 99;
        c = a + b;
        System.out.println(c);

        String d, e, f;
        d="Hell, ";
        e="utilizador";
        f= d+e;
        System.out.println(f);
    }
}
