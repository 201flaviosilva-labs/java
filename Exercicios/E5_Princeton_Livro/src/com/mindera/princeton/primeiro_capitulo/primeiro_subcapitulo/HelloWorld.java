package com.mindera.princeton.primeiro_capitulo.primeiro_subcapitulo;

public class HelloWorld {
    public static void main(String[] args) {
        // Prints "Hello, World" in the terminal window.
        System.out.println("Hello, World");
    }
}
