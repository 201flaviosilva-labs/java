package com.mindera.princeton.primeiro_capitulo.primeiro_subcapitulo;

public class Iniciais {

    public static void main(String[] args) {
        System.out.println("**        ***    **********      **             *             **");
        System.out.println("**      ***      **        **     **           ***           ** ");
        System.out.println("**    ***        **         **     **         ** **         **  ");
        System.out.println("**  ***          **          **     **       **   **       **   ");
        System.out.println("*****            **          **      **     **     **     **    ");
        System.out.println("**  ***          **          **       **   **       **   **     ");
        System.out.println("**    ***        **         **         ** **         ** **      ");
        System.out.println("**      ***      **        **           ***           ***       ");
        System.out.println("**        ***    **********              *             *        ");
    }
}
