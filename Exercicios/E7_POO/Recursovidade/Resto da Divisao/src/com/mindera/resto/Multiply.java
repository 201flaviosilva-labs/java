package com.mindera.resto;

public class Multiply {
    public static int multiplication(int num1, int num2) {
        if (num1 == 0 || num2 == 0) {
            return 0;
        } else if (num1 == 1 || num2 == 1) {
            if (num1 == 1) return num2;
            else return num1;
        } else if (num2 > 0) {
            return num1 + multiplication(num1, num2 - 1);
        } else {
            return -num1 + multiplication(num1, num2 + 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(multiplication(0, 2)); // 0
        System.out.println(multiplication(2, 0)); // 0
        System.out.println(multiplication(1, 2)); // 2
        System.out.println(multiplication(2, 1)); // 2
        System.out.println(multiplication(2, 3)); // 6
        System.out.println(multiplication(-2, 3)); // -6
        System.out.println(multiplication(2, -3)); // -6
    }
}

