package com.mindera.resto;

public class Resto {
    public static void main(String[] args) {
        System.out.println(resto(11, 2));
        System.out.println(resto(12, 12));
        System.out.println(resto(43, 23));
        System.out.println(resto(1567, 43));
        System.out.println(resto(1, 50));
    }

    static int resto(int x, int y){
        if (x == 0 || y == 0 || x == y) {
            return 1;
        } else if (x > 0 && x < y) {
            return x;
        }
        else if (x > 0) {
            return resto( (x-y), y);
        }
        // para x negativo
        else {
            return resto( (x-y), y);
        }

    }
}
