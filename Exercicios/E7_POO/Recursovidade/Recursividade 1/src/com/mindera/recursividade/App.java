package com.mindera.recursividade;

public class App {
    public static void main(String[] args) {
        System.out.println(fact(5));

    }
    static int fact(int num){
        if (num == 0) {
            System.out.println("0");
            return 1;
        }else{
            System.out.println(num);
            return num * fact(num-1);
        }
    }
}
