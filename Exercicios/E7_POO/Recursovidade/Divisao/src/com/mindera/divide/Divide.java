package com.mindera.divide;

public class Divide {
    public static void main(String[] args) {
        System.out.println(divisao(11, 2));
        System.out.println(divisao(14, 3));
        System.out.println(divisao(15, 5));
        System.out.println(divisao(20, 45));
        System.out.println(divisao(45, 9));
        System.out.println(divisao(111, 2));
    }

    static int divisao(int x, int y) {
        if (x == 0) {
            return 0;
        } else if (x < y) {
            return 0;
        } else {
            return 1 + divisao((x - y), y);
        }
    }
}

