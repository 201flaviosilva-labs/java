package com.mindera.music;

public class Music {
    String name;
    int duration;
    Artist artist;

    public Music(String name, int duration, Artist artist) {
        this.name = name;
        this.duration = duration;
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "Music{" +
                "name='" + name + '\'' +
                ", duration=" + duration +
                ", artist=" + artist +
                '}';
    }
}
