package com.mindera.music;

import java.util.LinkedList;

public class Album {
    String name;
    LinkedList<Music> musics = new LinkedList<>();
    Artist artist;

    public int count(){
        return musics.size();
    }

    public Album(String name, LinkedList<Music> musics, Artist artist) {
        this.name = name;
        this.musics = musics;
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", musics=" + musics +
                ", artist=" + artist.name +
                '}';
    }
}
