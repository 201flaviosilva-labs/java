package com.mindera.music;

public class Artist {
    String name;
    int age;
    String description;

    public Artist(String name, int age, String description) {
        this.name = name;
        this.age = age;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", description='" + description + '\'' +
                '}';
    }
}
