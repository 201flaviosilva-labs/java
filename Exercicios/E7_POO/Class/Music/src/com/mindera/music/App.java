package com.mindera.music;

import java.util.LinkedList;

public class App {
    public static void main(String[] args) {
        System.out.println("HEEEEY Musics");
        LinkedList<Artist> artist = new LinkedList<>();
        artist.add(new Artist("Diabo na Cruz", 10, "Gajos porreiros bem fixolas"));
        artist.add(new Artist("Nirvana", 27, "Gajos porreiros bem fixolas"));

        LinkedList<Music> musicsLebre = new LinkedList<>();
        musicsLebre.add(new Music("Forte", 200, artist.get(0)));
        musicsLebre.add(new Music("Procissão", 250, artist.get(0)));
        musicsLebre.add(new Music("Roquete da Casa", 183, artist.get(0)));
        musicsLebre.add(new Music("Terra Natal", 200, artist.get(0)));
        musicsLebre.add(new Music("Balada", 207, artist.get(0)));
        musicsLebre.add(new Music("Terra Ardida", 200, artist.get(0)));
        musicsLebre.add(new Music("Tema da Lebre", 67, artist.get(0)));
        musicsLebre.add(new Music("Malhão 3.0", 209, artist.get(0)));
        musicsLebre.add(new Music("Montanha Mãe / Contramão", 446, artist.get(0)));
        musicsLebre.add(new Music("Lebre", 101, artist.get(0)));
        musicsLebre.add(new Music("Portugal", 313, artist.get(0)));

        LinkedList<Music> musicsNevermind = new LinkedList<>();
        musicsNevermind.add(new Music("Smells like Teen Spirits", 200, artist.get(1)));
        musicsNevermind.add(new Music("In bloodm", 250, artist.get(1)));
        musicsNevermind.add(new Music("Come As You Are", 183, artist.get(1)));
        musicsNevermind.add(new Music("Breed", 200, artist.get(1)));
        musicsNevermind.add(new Music("Lithium", 207, artist.get(1)));
        musicsNevermind.add(new Music("Polly", 200, artist.get(1)));
        musicsNevermind.add(new Music("Terrotirial Pissing", 67, artist.get(1)));
        musicsNevermind.add(new Music("Drain You", 209, artist.get(1)));
        musicsNevermind.add(new Music("Lounge Act", 446, artist.get(1)));
        musicsNevermind.add(new Music("Stay Away", 101, artist.get(1)));
        musicsNevermind.add(new Music("On A Plain", 313, artist.get(1)));
        musicsNevermind.add(new Music("Something In The Way", 313, artist.get(1)));
        musicsNevermind.add(new Music("Endless, Nameless", 313, artist.get(1)));


        Album lebre = new Album("Lebre", musicsLebre, artist.get(0));
        Album nevermind = new Album("Nevermind", musicsNevermind, artist.get(1));

        System.out.println();
        System.out.println("Artista");
        System.out.println(artist.get(0).toString());
        System.out.println(artist.get(1).toString());

        System.out.println();
        System.out.println("Musics");
        System.out.println("Lebre");
        for (Music musicasFor : musicsLebre) {
            System.out.println(musicasFor.toString());
        }
        System.out.println("Nevermind");
        for (Music musicasFor : musicsNevermind) {
            System.out.println(musicasFor.toString());
        }

        System.out.println();
        System.out.println("Album");
        System.out.println(lebre.toString());
        System.out.println(nevermind.toString());


    }
}
