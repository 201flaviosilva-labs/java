import java.util.Scanner;


public class SchoolApp {
    public static final int MAX_NUMBER_OF_STUDENTS = 100;
    public static Scanner keyboard = new Scanner(System.in);

    public static int count = 0;
    public static String[] names = new String[MAX_NUMBER_OF_STUDENTS];
    public static int[] ages = new int[MAX_NUMBER_OF_STUDENTS];
    public static float[] grades = new float[MAX_NUMBER_OF_STUDENTS];

    public static void insertStudent() {
        String name = ConsoleAppUtils.askString("Name");

        int age = ConsoleAppUtils.askInt("Age");
        if (age <= 0) {
            return;
        }

        float grade = ConsoleAppUtils.askFloat("Grade");
        if (grade < 0 || grade > 20) {
            return;
        }

        addStudent(name, age, grade);
    }

    public static void printStudent(int index) {
        System.out.println("Name: " + names[index] + "\tAge: " + ages[index]+ "\tGrade " + grades[index]);
    }

    public static void addStudent(String name, int age, float grade) { 
        names[count] = name;
        ages[count] = age;
        grades[count] = grade;
        count++;
    }

    public static int findMax(float[] arr, int size) {
        if (arr.length == 0) {
            return -1;
        }

        int maxIndex = 0;
        float currentMax = arr[maxIndex];

        for(int i=1; i < size; ++i) {
            if (arr[i] > currentMax) {
                maxIndex = i;
                currentMax = arr[i];
            }
        }

        return maxIndex;
    }

    public static void printReport() {
        for(int i=0; i < count; ++i) {
            printStudent(i);
        }

        int maxGradeStudentIndex = findMax(grades, count);
        System.out.println("Student with highest grade");
        printStudent(maxGradeStudentIndex);
    }

    public static void main(String[] args) {
        int option;
        
        do {
            option = ConsoleAppUtils.printMenu(new String[] {
                "Insert Student",
                "Print Report",
                "Exit"
            });

            switch(option) {
                case 1:
                    insertStudent();
                    break;
                case 2:
                    printReport();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Invalid option");
            }
        } while(option != 3);
    }
}