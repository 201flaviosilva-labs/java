import java.util.Scanner;
public class Aluno{

	// Zona de Variaveis
	public static int count1 = 0;

	public static Scanner receber = new Scanner(System.in);

	public static int maximoAlunos = 100;
	public static String[] nomes = new String [maximoAlunos];
	public static int[] idade = new int [maximoAlunos];
	public static float[] nota = new float [maximoAlunos];

	public static void main(String[] args) {
		System.out.println("Escola: ");
		System.out.println("");

		int escolhaMenu;
		do{ //While
			System.out.println("1 -> Inserir um novo aluno");
			System.out.println("2 -> Imprimir relatório");
			System.out.println("3 -> Sair");
			System.out.println("");
			System.out.print("Opção: ");
			escolhaMenu = receber.nextInt();
			System.out.println("");

			switch(escolhaMenu){
				case 1:
					System.out.println("Inserir novo aluno");
					inserirAlunoMethod();
					System.out.println("");
					break;
				case 2:
					System.out.println("Imprimir relatório");
					relatorioMethod();
					System.out.println("");
					break;
				case 3:
					System.out.println("Escolhes-te sair");
					System.out.println("");
					break;
			}// switch, case
		} while (escolhaMenu != 3); //end While
		System.out.println("Bye!");
	}//end class

	public static void inserirAlunoMethod(){

		System.out.println("");
		System.out.print(count1 + " Nome: ");
		nomes[count1] = receber.next();
		System.out.println("");

		System.out.print(count1 + " Idade: ");
		idade[count1] = receber.nextInt();
		if (idade[count1] < 0) {
			System.out.println("Error -> Novo de mais para entrar");
			count1--;
		}//end if
		System.out.println("");

		System.out.print(count1 + " Nota: ");
		nota[count1] = receber.nextFloat();
		if (nota[count1] < 0) {
			System.out.println("Error -> Nota inaceitavel");
			count1--;
		}//end if
		System.out.println("");
		count1++;
	} //end inserirAlunoMethod


	public static void relatorioMethod(){
		System.out.println("");
		float maiorNota = 0;
		int id_AlunoNotaMaior = 0;
		float media = 0;
		for (int count2 = 0; count2 < count1 ; count2++) {
			System.out.println(count2 + " Nome: " + nomes[count2] + " Idade: " + idade[count2] + " Média: " + nota[count2]);
			if (maiorNota < nota[count2]) {
				maiorNota = nota[count2];
				id_AlunoNotaMaior = count2;
				media =+ nota[count2];
			} // end if
		} //end for
		System.out.println("");
		System.out.println("Aluno com a mior nota da escola: ");
		System.out.println("Nome: " + nomes[id_AlunoNotaMaior] + " Idade: " + idade[id_AlunoNotaMaior] + " Média: " + nota[id_AlunoNotaMaior]);
		System.out.println("Média da escola: " + media);
	}//end relatorioMethod
}//end class
