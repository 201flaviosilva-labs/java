package com.mindera.filmes;

import java.time.LocalDate;

public class Realizador {
    private int id;
    private String nome;
    private LocalDate anoNascimento;

    public Realizador(String nome, LocalDate anoNascimento) {
        this.nome = nome;
        this.anoNascimento = anoNascimento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getAnoNascimento() {
        return anoNascimento;
    }

    public void setAnoNascimento(LocalDate anoNascimento) {
        this.anoNascimento = anoNascimento;
    }
}
