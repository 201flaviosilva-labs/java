package com.mindera.filmes;

public class Personagem {
    private String nome;
    private String papel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Personagem that = (Personagem) o;

        if (getNome() != null ? !getNome().equals(that.getNome()) : that.getNome() != null) return false;
        return getPapel() != null ? getPapel().equals(that.getPapel()) : that.getPapel() == null;
    }

    @Override
    public int hashCode() {
        int result = getNome() != null ? getNome().hashCode() : 0;
        result = 31 * result + (getPapel() != null ? getPapel().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Personagem{" +
                "nome='" + nome + '\'' +
                ", papel='" + papel + '\'' +
                '}';
    }

    public Personagem(String nome, String papel) {
        this.nome = nome;
        this.papel = papel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPapel() {
        return papel;
    }

    public void setPapel(String papel) {
        this.papel = papel;
    }
}
