package com.mindera.filmes;

public class Ator {
    private int id;
    private String nomeAtor;
    private int anoNascimentoAtor;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ator ator = (Ator) o;

        if (getId() != ator.getId()) return false;
        if (getAnoNascimentoAtor() != ator.getAnoNascimentoAtor()) return false;
        return getNomeAtor() != null ? getNomeAtor().equals(ator.getNomeAtor()) : ator.getNomeAtor() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getNomeAtor() != null ? getNomeAtor().hashCode() : 0);
        result = 31 * result + getAnoNascimentoAtor();
        return result;
    }

    @Override
    public String toString() {
        return "Ator{" +
                "id=" + id +
                ", nomeAtor='" + nomeAtor + '\'' +
                ", anoNascimentoAtor=" + anoNascimentoAtor +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeAtor() {
        return nomeAtor;
    }

    public void setNomeAtor(String nomeAtor) {
        this.nomeAtor = nomeAtor;
    }

    public int getAnoNascimentoAtor() {
        return anoNascimentoAtor;
    }

    public void setAnoNascimentoAtor(int anoNascimentoAtor) {
        this.anoNascimentoAtor = anoNascimentoAtor;
    }
}
