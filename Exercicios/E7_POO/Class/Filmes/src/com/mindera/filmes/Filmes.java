package com.mindera.filmes;

public class Filmes {
   private String nome;
   private   String sinops;

   private Genero[] genero;
   private Realizador[] realizador;
   private Premios[] premio;
   private Elenco elenco;
   private Personagem[] personagem;
   private Ator[] ator;

    public Filmes(String nome, String sinops, Genero[] genero, Realizador[] realizador, Premios[] premio, Elenco elenco, Personagem[] personagem, Ator[] ator) {
        this.nome = nome;
        this.sinops = sinops;
        this.genero = genero;
        this.realizador = realizador;
        this.premio = premio;
        this.elenco = elenco;
        this.personagem = personagem;
        this.ator = ator;
    }


}
