package com.mindera.school.cinema;

public class CinemaApp {
    public static void main(String[] args) {
        int option;
        
        do {
            option = ConsoleAppUtils.printMenu(new String[] {
                "Sell Ticket",
                "Print Room",
                "Exit"
            });

            switch(option) {
                case 1:
                    CinemaActions.sellTicket();
                    break;
                case 2:
                    CinemaActions.printRoom();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Invalid option");
            }
        } while(option != 3);
    }
}
