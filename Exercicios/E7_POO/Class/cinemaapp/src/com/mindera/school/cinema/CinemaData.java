package com.mindera.school.cinema;

public class CinemaData {
    private static boolean[][] seats = new boolean[30][40];

    public static boolean sellTicket(int row, int seatNumber) {
        if (row >= getNumberOfRows()) {
            return false;
        }

        if (seatNumber >= getNumberOfSeatsPerRow()) {
            return false;
        }

        boolean isSold = seats[row][seatNumber];

        if (isSold) {
            return false;
        }

        seats[row][seatNumber] = true;

        return true;
    }

    public static boolean isSold(int row, int seatNumber) {
        return seats[row][seatNumber];
    }

    public static int getNumberOfRows() {
        return seats.length;
    }

    public static int getNumberOfSeatsPerRow() {
        return seats[0].length;
    }


    public static boolean[][] importations() {
        return seats;
    }
}