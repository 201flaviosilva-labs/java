package com.mindera.school.cinema;

public class CinemaUI {

    public static int[] askSeat() {
        int row = ConsoleAppUtils.askInt("Row");
        int seatNumber = ConsoleAppUtils.askInt("seatNumber");
        
        return new int[] { row, seatNumber };
    }

    public static void showErrorMessage(String msg) {
        System.out.println(msg);
    }
}
