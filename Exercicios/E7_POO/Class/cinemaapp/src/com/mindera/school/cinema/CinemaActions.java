package com.mindera.school.cinema;

public class CinemaActions {
    
    public static void sellTicket() {
        int[] seat = CinemaUI.askSeat();
        boolean ok = CinemaData.sellTicket(seat[0], seat[1]);
        
        if (!ok) {
            CinemaUI.showErrorMessage("Could not sell this ticket");
        }
    }

    public static void printRoom() {
        boolean[][] seats = CinemaData.importations();
        for (int count1 = 0; count1 < seats.length; count1++){
            for (int count2 = 0; count2 < seats[count1].length; count2++) {
                if (seats[count1][count2] == true){
                    System.out.print("x ");
                }//end if
                else {
                    System.out.print("0 ");
                } //end else
            } // end for
            System.out.println("");
        } // end for
    } // printRomm
} //end class
