package com.mindera.salaappp;

public class Aluno {
    int aluno_id;
    int idade;
    String nome;

    Aluno(int id, int idade, String nome) {
        this.aluno_id = id;
        this.idade = idade;
        this.nome = nome;
    }

   @Override
   public String toString() {
        return aluno_id + " aluno_id + " +  idade + " + idade + " + nome + " nome ";
    }

    public int getAluno_id() {
        return aluno_id;
    }

    public void setAluno_id(int aluno_id) {
        this.aluno_id = aluno_id;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
