package  com.mindera.calc;

public class Calculator {
    private double number = 0, digit = 0, result = 0;
    private int opration = 0;

    void readNum(double num) {
        if (opration == 0) {
            number = num;
        } else {
            this.digit = num;
        }
    }

    void calculation() {
        switch (opration) {
            case 1: //sum
                number = number + digit;
                break;
            case 2: //sub
                number = number - digit;
                break;
            case 3: //mult
                number = number * digit;
                break;
            case 4: //div
                number = number / digit;
                break;
        }
    }

    void sum() {
        calculation();
        opration = 1;
    }

    void sub() {
        calculation();
        opration = 2;
    }

    void mult() {
        calculation();
        opration = 3;
    }

    void div() {
        calculation();
        opration = 4;
    }

    void equals() {
        calculation();
        result = number;
        number = 0;
        opration = 0;
    }

    public double getResult() {
        return result;
    }

    public static void main(String[] args) {
        Calculator c = new Calculator();
        c.readNum(3);
        c.sum();
        c.readNum(3);
        c.mult();
        c.readNum(6);
        c.div();
        c.readNum(2);
        c.sub();
        c.readNum(4);

        c.equals();
        System.out.println(c.getResult());
    }
}
