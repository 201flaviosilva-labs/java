package com.mindera.linklist;

public class MyLinkList<T> {

    private Node<T> root;

    public void set(T value) {
        if (root == null) {
            root = new Node<>(value);
        } else {
            add(root, value);
        }
    }

    public void add(Node corent, T value) {
        if (corent.next == null) {
            corent.next = new Node<T>(value);
        } else {
            add(corent.next, value);
        }
    }
}
