package com.mindera.linklist;

public class LinkedList<T> {
    Node<T> root;

    public void add(T value) {
        if (this.root == null) {
            this.root = new Node<>(value);
        } else {
            Node<T> tmp = this.root;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
            tmp.next = new Node<>(value);
        }
    }

    public Node<T> get(int index) {
        Node<T> tmp = this.root;
        if (this.root == null) {
            System.out.println("é nulo");
            return null;
        } else {
            for (int i = 0; i < index; i++) {
                tmp = tmp.next;
            }
        }
        return tmp;
    }

    public static void main(String[] args) {
        LinkedList<Integer> l = new LinkedList<>();
        l.add(1);
        l.add(5);
        l.add(7);
        l.add(9);
        l.add(78);
        Node joaozinho = l.get(0);
        System.out.println(joaozinho);
    }
}
