package com.mindera.linklist;

public class Node<T> {
    T value;
    Node<T> next;

    Node(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node {" +
                "value =" + value +
                ",next =" + next +
                "}";
    }
}
