package com.mindera.doublelinkedlist;

public class DoubleLinkedList<T> {
    Node<T> root;

    public void add(T value) {
        if (this.root == null) {
            this.root = new Node<>(value);
        } else {
            Node<T> tmp = this.root;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
            Node<T> tmp2 = new Node<>(value);
            tmp.next = tmp2;
            tmp2.previous = tmp;
        }
    }

    public Node<T> get(int index) {
        Node<T> tmp = this.root;
        if (this.root == null) {
            System.out.println("é nulo");
            return null;
        } else {
            for (int i = 0; i < index; i++) {
                tmp = tmp.next;
            }
        }
        return tmp;
    }

    public static void main(String[] args) {
        DoubleLinkedList<Integer> l = new DoubleLinkedList<>();

        l.add(0);
        l.add(1);
        l.add(2);

        Node joaozinho = l.get(2);
        System.out.println(joaozinho);
    }
}
