package com.mindera.doublelinkedlist;

public class Node<T> {
    T value;
    Node<T> next;
    Node<T> previous;

    Node(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value = " + value +
               // ", next = " + next +
                ", previous = " + previous +
                '}';
    }
}
