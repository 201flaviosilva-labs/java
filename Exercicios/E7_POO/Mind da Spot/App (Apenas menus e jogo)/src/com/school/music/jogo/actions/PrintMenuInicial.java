package com.school.music.jogo.actions;

import com.school.music.school.music.ui.Action;
import com.school.music.jogo.QuestionAction;

public class PrintMenuInicial implements Action {
    @Override
    public void execute() {
        new QuestionAction("", new String[]{"Jogar", "Sair"}, new Action[]{new ZombieLand(), new SairAction()});
    }
}
