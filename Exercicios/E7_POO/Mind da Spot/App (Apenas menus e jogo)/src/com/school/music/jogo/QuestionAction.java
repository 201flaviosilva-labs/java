package com.school.music.jogo;

import com.school.music.school.music.ui.Action;
import com.school.music.school.music.ui.Menu;
import com.school.music.school.music.ui.Option;

public class QuestionAction implements Action {
    public String question;
    public Menu menu;

    public QuestionAction(String question, String[] answers, Action[] action) {
        this.question = question;

        menu = new Menu();

        if (answers.length == action.length) {
            for (int i = 0; i < answers.length; i++) {
                menu.add(new Option(answers[i], action[i]));
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void execute() {
        System.out.println(question);
        menu.render();
    }
}
