package com.school.music.school.music.ui;

public interface Action {
    void execute();
}
