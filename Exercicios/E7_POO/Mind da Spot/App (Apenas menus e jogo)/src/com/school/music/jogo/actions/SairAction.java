package com.school.music.jogo.actions;

import com.school.music.school.music.ui.Action;

public class SairAction implements Action {
    @Override
    public void execute() {
        System.exit(0);
    }
}
