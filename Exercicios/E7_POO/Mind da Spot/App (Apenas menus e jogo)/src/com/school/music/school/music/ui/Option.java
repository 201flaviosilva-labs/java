package com.school.music.school.music.ui;

public class Option {
    private String text;
    private Action action;

    public Option(String text, Action action) {
        this.text = text;
        this.action = action;
    }

    public String getText(){
        return text;
    }

    void execute() { action.execute();}

    @Override
    public String toString() {
        return " - " + text;
    }
}
