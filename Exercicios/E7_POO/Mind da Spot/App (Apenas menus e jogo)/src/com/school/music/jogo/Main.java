package com.school.music.jogo;

import com.school.music.jogo.actions.IniciarJogoAction;
import com.school.music.jogo.actions.SairAction;
import com.school.music.school.music.ui.Action;

public class Main {
    public static void main(String[] args) {
        /*
        new QuestionAction("Zombieland",
                new String[]{"Jogar","Sair"},
                new Action[]{new IniciarJogoAction(), new SairAction()}).execute();
                */

        new QuestionAction("Zombieland",
                new String[]{"Jogar","Sair"},
                new Action[]{new IniciarJogoAction(), new SairAction()}).execute();

    }
}
