public class JavaMath{
	public static void main(String[] args) {
		int num1=5, num2=15;

		int numMaximo= Math.max(num1, num2); //Mostra o maior dos dois números
		System.out.println(numMaximo);

		int numMinimo= Math.min(num1, num2); //Mostra o menor dos dois números
		System.out.println(numMinimo);

		double numQuadrado= Math.sqrt(64); //o quadrado de 64 -> 8
		System.out.println(numQuadrado);

		int numPositivo= Math.abs(-5); //Mostra o positivo
		System.out.println(numMinimo);

		System.out.println(Math.random());//cria um número aletório de 0 a 0.99999

		int numAleatorio=(int) (Math.random()*6)+1; //cria um número aletório de 1 a 6
		System.out.println(numAleatorio);
	}
}