public class FuncoesStringes{
	public static void main(String[] args) {
		String palavra="aBcDEf";
		int tamanho=palavra.length();
		System.out.println(tamanho);
		System.out.println(palavra.toUpperCase());   // Outputs "ABCDEF"
		System.out.println(palavra.toLowerCase());	// Outputs "abcdef"
		String palavra2=palavra.toLowerCase();
		System.out.println(palavra2);

		System.out.println( "We are the so-called \"Vikings\" from the north."); //-> " é apresentada
		System.out.println( "It\'s alright."); //-> ' é apresentada
		System.out.println("The character \\ is called backslash."); //-> / é apresentada
	}//end method
}//end class