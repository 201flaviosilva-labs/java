public class DeclaracaoVariaveis{
	public static void main(String[] args) {
		/*
		String - stores text, such as "Hello". String values are surrounded by double quotes
		int - stores integers (whole numbers), without decimals, such as 123 or -123
		float - stores floating point numbers, with decimals, such as 19.99 or -19.99
		char - stores single characters, such as 'a' or 'B'. Char values are surrounded by single quotes
		boolean - stores values with two states: true or false
		*/
		//tipo nomeVariavel = Valor;

		String palavras="Flávio"; //Varia que permite guardar conjunto de letras
		String ultimoNome="Silva"; //Outra variavel de tipo String
		String nomeCompleto=palavras+" "+ultimoNome; //Junção de Variaveis
		int num1=18; //Varia que permite guardar número insteiros
		double num2=31.10; //Varia que permite guardar números com virgulas
		char letra='S'; //Varia que permite guardar uma letra
		boolean simNao=true;//variavel que apenas permite qualdar variaveis true ou false
		int numero1=0, numero2=1, numero3=3;//criação de 3 variaveis de tipo inteiro
		int num4;//criação de variavel sem introdução de valor (sem iniciar)
		num4=10;//iniciação da variavel anteriormente criada;

		System.out.println("Nome: "+palavras);
		System.out.println("Último nome: "+ultimoNome);
		System.out.println("Idade: "+num1);
		System.out.println("Data de Namoro: "+num2);
		System.out.println("Letra do ultimo nome: "+letra);
		System.out.println("Juntar Nomes: "+palavras+ultimoNome);
		System.out.println("Juntar Nomes 2: "+nomeCompleto);
		System.out.println("Boolean"+simNao);
		System.out.println(numero1 + numero2 + numero3);
		System.out.println(num4);

	}//end method
}//end class