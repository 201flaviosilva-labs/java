public class GerarNumeroAleatorio {

    public static void main(String[] args) {
        
        System.out.println("Este programa irá gerar varios números aleatórios");
        //rnd=é uma variavel que criei
        double rnd =Math.random();
        
        //Entre 0 a 10
        int num1= (int) ((10)*rnd);
        
        //Entre 5 a 10
        int num2 =(int) ((5)*rnd+5);
        
        System.out.println("O número aleatório gerado (entre 0 e 1) é: " + rnd);
        System.out.println("De 0 a 10: " + num1);
        System.out.println("De 5 a 10: " + num2);
        System.out.println("------------------------------------");
    }
    
}
