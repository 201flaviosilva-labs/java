/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4.operadores;

/**
 *
 * @author flavi_000
 */
public class Operadores {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Operadores Aritméticos
        int num1 =10;
        int num2 =20;
        int soma= num1+num2;
        int subtração=num1-num2;
        int multiplicação = num1*num2;
        double divisao = num1/num2;
        int resto = num1%num2;
        
        System.out.println("Soma " + soma);
        System.out.println("Subtração " +subtração);
        System.out.println("Multiplicação " +multiplicação);
        System.out.println("Divisão " +divisao);
        System.out.println("Resto " +resto);
    }
    
}
