public class OperadoresLogicos{
   public static void main (String[] args){
       int idade =18;
       float salario =1000f;
       
       //&& = "E"
       System.out.println(idade>=18 && salario>=3000);
       
        //|| = "ou"
       System.out.println(idade>=18 && salario>=3000);
    }
}
