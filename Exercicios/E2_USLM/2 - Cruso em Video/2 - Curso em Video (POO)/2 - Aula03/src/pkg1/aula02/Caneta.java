package pkg1.aula02;
//Criação da calss "Caneta"
public class Caneta {   
    //Criação dos atributos - O que a compõem
    public String modelo;
    public String cor;
    private float ponta;
    protected int carga;
    protected boolean tampada;
    
    //Criação de uma sub rotina - rotina de diagnostico de estádo
    public void status(){
        System.out.println("Modelo: " + this.modelo);
        System.out.println("Uma caneta de cor: " + this.cor);
        System.out.println("Pont: " + this.ponta);
        System.out.println("Carga: " + this.carga);
        System.out.println("Está tampada? " + this.tampada);  
    }
    
    //Rotina de se a tampa está fora ou não
   public void rabiscar (){
        if (this.tampada == true){
            System.out.println("Erro! Não posso rabiscar!");
        } else {
            System.out.println("Estou rabiscando!");
        }
    } 
   
   //Sub-rotinas de tampar ou não a tampa da caneta
    protected void tampar(){
        this.tampada=true;
    }
    protected void destampar (){
        this.tampada=false;
    }
}