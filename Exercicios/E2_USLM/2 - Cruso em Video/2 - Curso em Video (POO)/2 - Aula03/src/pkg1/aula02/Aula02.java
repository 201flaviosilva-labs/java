package pkg1.aula02;
public class Aula02 {
    public static void main(String[] args) {
        //Atribuição de valores ás variaveis
        Caneta caneta1 =new Caneta();
        caneta1.modelo="Bic";
        caneta1.cor="Azul";
        
        //Variavel privada
        //caneta1.ponta=0.5f;
        
        caneta1.carga=80;
        caneta1.tampada=true;
        
        //Chamamento de sub rotinas
        caneta1.status();
        caneta1.rabiscar();
    } 
}
