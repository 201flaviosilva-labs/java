package exercicio.pkg2;
public class Conta {
    public String numConta;
    protected String tipo;
    private String dono;
    private float saldo;
    private boolean status;

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDono() {
        return dono;
    }

    public void setDono(String dono) {
        this.dono = dono;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
     
    void AbrirConta(){
        this.status=true;
        
    }
    
    void FecharConta(){
        if ((saldo==0) && (this.status=true)){
         this.status=false;   
        }
    }
    
    void depositar(){
        
    }
}