/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testetipos;

/**
 *
 * @author flavi_000
 */
public class TesteTipos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int idade1 = 30;
        String valor1 =Integer.toString(idade1);
        System.out.println("O valor 1 é: " + valor1);
        
        
        String valor2 = "40";
        int idade2 = Integer.parseInt(valor2);
        System.out.println("O valor 2 é: " + idade2);
        
        String valor3 = "50.67";
        double idade3 = Double.parseDouble(valor3);
        System.out.println("O valor 3 é: " + idade3);
    }
    
}
