/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operadoresaritmeticos;

/**
 *
 * @author flavi_000
 */
public class OperadoresAritmeticos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Este programa vai fazer a média entre dois números");
        int num1 = 3;
        int num2 = 5;
        double media =(num1 + num2)/2;
        System.out.println("A média é de: " + media);
        
        
        //Outros Exercicios
        double num3=5;
        byte num4= 10; 
        double num5=20;
        double resultado1 =1;
        double resultado2=1;
        double num6=3;
        // ++ = Somar +1 ao antigo valor
        num3++;
        System.out.println("O número 3 é: " +num3);
        //-- = Subtrair -1 ao antigo valor
        num4--;
        System.out.println("O número 4 é: " + num4);
        
        //5 + num5++ = não irá resultar pois o Java faz primeiro a conta e só depois é que adiciona +1
        resultado1=  (5 + num5++);
        System.out.println("O valor do número 5 é: " + num5);
        System.out.println("O 'valor' + num5++ é: " + resultado1);
        
         //5 + ++num6 = já irá resultar pois o Java faz primeiro a soma do num6 +1 e depois a conta
        resultado2= (5 + ++num6);
        System.out.println("O valor do número 5 é: " + num6);
        System.out.println("O 'valor' + num5++ é: " + resultado2);
        
        
        
        //Ainda mais outros exercicios
        byte x = 1;
        x+=2; // isto é x=x+2;
        System.out.println("O valor de x é: "+x);
        
    }
    
}
