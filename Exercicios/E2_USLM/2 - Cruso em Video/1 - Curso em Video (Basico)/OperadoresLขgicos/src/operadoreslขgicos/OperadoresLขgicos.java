/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operadoreslógicos;

/**
 *
 * @author flavi_000
 */
public class OperadoresLógicos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x,y, z;
        x=4;
        y=7;
        z=12;
        boolean resultado1, resultado2, resultado3, resultado4;
        
        //&&="e"
        resultado1 =(x<y && y<z) ? true: false;
        System.out.println(resultado1);
        
        //==  ="Igual"
        resultado2 =(x<y && y==z) ? true: false;
        System.out.println(resultado2);
        
        //|| ="ou"
        resultado3 =(x<y || y==z) ? true: false;
        System.out.println(resultado3);
        
        //^ = "ou exclusivo de uma das soluções"
        resultado4 =(x<y ^ y<z) ? true: false;
        System.out.println(resultado4);
    }
    
}
