/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contador01.ciclos;

/**
 *
 * @author flavi_000
 */
public class Contador01Ciclos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int count=0;
        while (count<10){
            count++;
            if ((count==2) || (count==3)){
                //O continue tem a funsão de voltar ao inicio do ciclo
                continue;
            }
            if (count==7) {
                //break = uma quebra no ciclo
                break;
            }
            System.out.println("Cambalhota " + count);
        }
    }
    
}
