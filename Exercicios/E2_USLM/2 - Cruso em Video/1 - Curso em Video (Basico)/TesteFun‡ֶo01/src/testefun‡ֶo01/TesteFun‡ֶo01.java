package testefunção01;

public class TesteFunção01 {

    //Sub-Rotina Publica Statica(Acho que significa que é só para esta classe), inteiro, nome da sub-rotina
   public static int soma (){
       //Introdução de valores
        int a =3;
        int b=8;
        //Soma
       int s=a+b;
        System.out.println("A soma é : " + s);
        //Guarda na memória s
       return s;
    }
    
   //Procedimento Principal, o programa vai começar aqui
    public static void main(String[] args) {
        //inicio do programa
        System.out.println("Começou o programa!");
        //Chamamento de sub Rotina
        soma(); 
        //Acho que faz a mesma coisa que o "s"
        System.out.println("A soma é : " + soma());
    }
    
}
