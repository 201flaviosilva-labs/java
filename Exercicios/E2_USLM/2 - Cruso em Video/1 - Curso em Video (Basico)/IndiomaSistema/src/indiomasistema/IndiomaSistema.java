package indiomasistema;
import java.util.*;
public class IndiomaSistema {

    public static void main(String[] args) {
        Locale loc= Locale.getDefault();
        System.out.println("O Indioma do sistema é: ");
        System.out.println(loc.getDisplayLanguage());
        System.out.println("Ou então: ");
        System.out.println(loc.getLanguage());
    }
    
}
