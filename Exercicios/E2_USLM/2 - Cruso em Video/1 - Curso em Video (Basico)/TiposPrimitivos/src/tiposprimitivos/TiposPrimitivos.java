/*
 * Comentário
 * De
 * Multiplas
 * Linhas
 */
package tiposprimitivos;

import java.util.Scanner;

/**
 *
 *@Comantário de Docomentação
 */
public class TiposPrimitivos {
    public static void main(String[] args) {
        // Comentário de uma linha
        System.out.println("Este Programa Vai te pedir o teu nome e a tua nota no último teste");
        
        Scanner teclado= new Scanner (System.in);
        //Indicação de que haverá interáção como utilizador
        System.out.print("Diz O teu Nome: ");
        String nome = teclado.nextLine();
        //Ler "nome"
        System.out.print("Diz a tua nota: ");
        float nota = teclado.nextFloat();
        //ler "nota"
        
        //Saidas de texto
        System.out.println("A nota é " + nota);
        //Saidas de texto formatadas
        // %f = a variavel nota irá para %f
        System.out.printf("A nota é %f ", nota);
        // /n = println
        System.out.printf(" A nota é %f \n", nota);
        // %.3f (.3)= número de casas decimais
        System.out.printf("A nota é %.3f \n", nota);
        // irá dizer o nome e a nota escolhidos pelo utilizador 
        System.out.printf("A nota de %s é %.2f \n", nome, nota);
        System.out.format("A nota de %s é %.1f \n", nome, nota);
    }
    
}
