/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gerarnumeroaleatorio;

/**
 *
 * @author flavi_000
 */
public class GerarNumeroAleatorio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Este programa irá gerar varios números aleatórios");
        //rnd=é uma variavel que criei
        double rnd =Math.random();
        
        //Entre 0 a 10
        int num1= (int) ((10)*rnd);
        
        //Entre 5 a 10
        int num2 =(int) ((5)*rnd+5);
        
        System.out.println("O número aleatório gerado (entre 0 e 1) é: " + rnd);
        System.out.println("De 0 a 10: " + num1);
        System.out.println("De 5 a 10: " + num2);
        System.out.println("------------------------------------");
    }
    
}
