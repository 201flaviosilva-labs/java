
package pkg11.whileloops;

public class WhileLoops {

    public static void main(String[] args) {
      int x = 0;
//inicio do ciclo
while(x<3) {
    //soma de x
    x++;
   //saida de x
   System.out.println(x);  
}//fim de Ciclo

/* 
Saida:
1
2
3
*/  
    }
    
}
