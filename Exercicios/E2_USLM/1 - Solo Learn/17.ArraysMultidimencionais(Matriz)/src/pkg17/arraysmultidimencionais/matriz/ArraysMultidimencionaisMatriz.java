package pkg17.arraysmultidimencionais.matriz;

public class ArraysMultidimencionaisMatriz {

    public static void main(String[] args) {
        //Matrizes
        int[ ][ ] sample = { {1, 2, 3}, {4, 5, 6} };
        //1,2,3 -> Linha
        //4,5,6 -> Coluna
        
        //1,4 - 1,5 - 1,6--------1-----4-5-6-;
        //2,4 - 2,5 - 2,6--------2-----4-5-6-;
        //3,4 - 3,5 - 3,6--------3-----4-5-6-;
        
        //1 =0,0; 4=0,0; 5=0,1; 6=0,2;
        //2 =1,0; 4=1,0; 5=1,1; 6=1,2;
        //3 =2,0; 4=2,0; 5=2,1; 6=2,2;
        
        int x = sample[1][0];
        System.out.println(x);
// Saida 4
                                 //O quatro é como se fosse o 0 de coluna (e o 5 passa a 1 da coluna)
int[ ][ ] myArr = { {1, 2, 3}, {4}, {5, 6, 7} };
    } 
}
