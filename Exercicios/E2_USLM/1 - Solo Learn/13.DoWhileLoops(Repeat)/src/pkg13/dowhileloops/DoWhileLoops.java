
package pkg13.dowhileloops;

public class DoWhileLoops {

    public static void main(String[] args) {
       int x = 0;
        do {
          x++;  
        System.out.println(x);
        } while(x < 5);     
/*
1
2
3
4
5
*/    
        int num1=0;
        do{
            num1++;
            System.out.println(num1);
            
            if (num1==8){
                //break é uma quebra no ciclo, para o ciclo de continuar
                break;
            }
    } while(num1<100);
    
}
}
