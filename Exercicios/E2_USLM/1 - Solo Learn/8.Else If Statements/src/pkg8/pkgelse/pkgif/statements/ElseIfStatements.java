
package pkg8.pkgelse.pkgif.statements;

public class ElseIfStatements {

    public static void main(String[] args) {
            //O else if = senão se
            int idade = 20;
            // se a idade for menor ou igual a 0 então
      if (idade <= 0){
          System.out.println("Algo está errado");
          // senão, se a idade for igual ou menor a 16 então
      }else if (idade <= 16){
          System.out.println("Demasiado Novo!");
          //senão se a idade mior que 100 então
      }else if (idade < 100){
          System.out.println("Bem vindo");
          //se não, então
      }else{
          System.out.println("Estás pronto?");
      } // end else
    } //
} // 
