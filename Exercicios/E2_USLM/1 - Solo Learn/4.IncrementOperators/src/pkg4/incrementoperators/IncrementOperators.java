
package pkg4.incrementoperators;

public class IncrementOperators {

    public static void main(String[] args) {
        int test1 = 5;
        ++test1; // test is now 6
        System.out.println(test1);
        
        int test2 = 5;
        --test2; // test is now 4
        System.out.println(test2);
        
        //outra forma de fazer o mesmo é:
        int test3 = 5;
        test3++; // test is now 6
        System.out.println(test3);
        
        int test4 = 5;
        test4--; // test is now 4
        System.out.println(test4);
       
        
        //Mas no segundo caso há um problema:
        int x1 = 34;
        int y1 = ++x1; // y is 35
        System.out.println(y1);
        
        int x2 = 34;
        int y2 = x2++; // y is 34
        System.out.println(y2);
        
        //Contas simplificadas
        int num1 = 4;
        int num2 = 8;
        num2 += num1; // num2 = num2 + num1;
        // num2 is 12 and num1 is 4
        //Resulta para qualquer operação
    }
    
}
