
package pkg3.opradoresprimários;

public class OpradoresPrimários {

    public static void main(String[] args) {
        /**
         * soma
         * int x = 6 + 3;
         * 
         * soma entre variaveis
         * int sum1 = 50 + 10; 
           int sum2 = sum1 + 66; 
           int sum3 = sum2 + sum2;
           * 
           * Subtração entre variaveis
           * int sum1 = 1000 - 10;
             int sum2 = sum1 - 5;
             int sum3 = sum1 - sum2;
             * 
             * 
             * Multiplicação entre variaveis
             * int sum1 = 1000 * 2;
               int sum2 = sum1 * 10;
               int sum3 = sum1 * sum2;
               * 
               * 
               * Divisaõe entre variaveis
               * int sum1 = 1000 / 5;
                 int sum2 = sum1 / 2;
                 int sum3 = sum1 / sum2;
                 * 
                 * 
                 * Oprador "Resto = %"
                 * int value = 23;
                   int res = value % 6; // resto é 5
         */
        
        /*exemplo
        int x = 8, y = 5;
        int result = x % y;
        System.out.println(result);
        */
    }
    
}
