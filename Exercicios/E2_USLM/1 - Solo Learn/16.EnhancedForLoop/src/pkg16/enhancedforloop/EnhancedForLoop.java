package pkg16.enhancedforloop;

public class EnhancedForLoop {

    public static void main(String[] args) {
       int[ ] primes = {2, 3, 5, 7};

       //O ciclo atravessa os elementos do ciclo 
for (int t: primes) {
   System.out.println(t); 
}

/*
2
3
5
7
*/
    }
    
}
