/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author flaviosilva
 */
public class TipoJogo {
    public static int dificuldade = 2;
    public static int nParagens = 18;

    public int getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    public int getnParagens() {
        return nParagens;
    }

    public void setnParagens(int nParagens) {
        this.nParagens = nParagens;
    }
    
    
}
