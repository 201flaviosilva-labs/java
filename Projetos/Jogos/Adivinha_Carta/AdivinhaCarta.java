import java.util.Scanner;
public class AdivinhaCarta {
	public static void main(String[] args) {

		Scanner receberString = new Scanner(System.in);

		System.out.println("-----------------------------");
		System.out.println("----- Adivinhar Número ------");
		System.out.println("-----------------------------");
		System.out.println("------ Olha o Baralho -------");
		System.out.println("-----------------------------");

		int corVerde = 20, corAmarelo = 16, corVermelho = 8, corAzul = 4, corBranco = 2, corPreto = 1;
		//pontuações -> Verdes = 1; Verdes = 2; Verdes = 4; Verdes = 8; Verdes = 16; Verdes = 20; 
		int numeroCartas = corVerde + corAmarelo + corVermelho + corAzul + corBranco + corPreto;

		String[] baralho = new String[numeroCartas];
		String[] cartasRandom = new String[6];

		int count3 = 0;

		for (int count1 = 0; count1 < corVerde; count1++) {
			baralho[count3]="verde";
			System.out.println(count3+" "+baralho[count3]);
			++count3;
		}//end for

		for (int count1 = 0; count1 < corAmarelo; count1++) {
			baralho[count3]="amarelo";
			System.out.println(count3+" "+baralho[count3]);
			++count3;
		}//end for

		for (int count1 = 0; count1 < corVermelho; count1++) {
			baralho[count3]="vermelho";
			System.out.println(count3+" "+baralho[count3]);
			++count3;
		}//end for

		for (int count1 = 0; count1 < corAzul; count1++) {
			baralho[count3]="azul";
			System.out.println(count3+" "+baralho[count3]);
			++count3;
		}//end for

		for (int count1 = 0; count1 < corBranco; count1++) {
			baralho[count3]="branco";
			System.out.println(count3+" "+baralho[count3]);
			++count3;
		}//end for

		for (int count1 = 0; count1 < corPreto; count1++) {
			baralho[count3]="preto";
			System.out.println(count3+" "+baralho[count3]);
			++count3;
		}//end for

		System.out.println("");
		System.out.println("-----------------------------");
		System.out.println("---- Vou baralhar, olha: ----");
		System.out.println("-----------------------------");
		System.out.println("");

		for (int count1 = 0; count1 < 6; count1++) {
			int cartaRandom=(int) (Math.random()*numeroCartas);
			cartasRandom[count1]=baralho[cartaRandom];
			System.out.println(cartasRandom[count1]);
		}//end for

		int cartaRandomEscolhida = (int) (Math.random()*6);

		System.out.println("-----------------------------");
		System.out.println("---------- Baralhei ---------");
		System.out.println("-----------------------------");
		System.out.println("- Agora comça a parte Gira --");
		System.out.println("-----------------------------");

		System.out.println("");
		System.out.println("Diz a cor da carta: ");
		System.out.print("Cor: ");
		String resposta = receberString.nextLine();
		resposta=resposta.toLowerCase();;//transforma todas as letras em minusculas
		System.out.println("");
		System.out.println("-----------------------------");
		System.out.println("");

		 

		//Mostrar se o jogador acertou
		System.out.println("Carta calhada: " + cartasRandom[cartaRandomEscolhida]);

		if (resposta.equals(cartasRandom[cartaRandomEscolhida])) {
			System.out.println("Muito bem acertates!");
		}//end if
		else {
			System.out.println("Pena falhaste!!");
		}//end else

		System.out.println("");

	}//end method
}//end Class
