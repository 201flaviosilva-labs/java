package com.planetagames.jogodogalo;

public class JogoDoGalo {
    public static char[][] tabuleiro = new char[3][3];
    public static boolean jogador = true;
    public static int nJogadas = 0;

    public static void main(String[] args) {
        inicio();

    }

    public static void inicio() {
        System.out.println("Tabuleiro");
        for (int i = 0; i < tabuleiro.length; i++) {
            for (int j = 0; j < tabuleiro[i].length; j++) {
                tabuleiro[i][j] = 'L';
            }
        }
        printTabuleiro();
        //do {

        System.out.println("A jogar: Jogador " + velificarJogador());
        boolean jogadaValida;
        // do jogadaValida = jogada(1, 2); while (jogadaValida);
        jogadaValida = jogada(1, 1); //Jogador1
        jogadaValida = jogada(2, 1); //Jog2
        jogadaValida = jogada(2, 2);  //Jog1
        jogadaValida = jogada(3, 1);//Jog2
        jogadaValida = jogada(3, 3);//Jog1
        // }while (verificarVitoria()==false);
    }

    public static void printTabuleiro() {
        for (int i = 0; i < tabuleiro.length; i++) {
            for (int j = 0; j < tabuleiro[i].length; j++) {
                System.out.print(" " + tabuleiro[i][j]);
            }
            System.out.println();
        }
    }

    public static int velificarJogador() {
        return (jogador ? 1 : 2);
    }


    public static boolean jogada(int linha, int coluna) {
        linha--;
        coluna--;
        boolean jogadaValida;
        if (tabuleiro[linha][coluna] == 'L') {
            if (jogador) {
                tabuleiro[linha][coluna] = 'X';
            } else {
                tabuleiro[linha][coluna] = 'O';
            }
            //tabuleiro[linha][coluna] = ((jogador) ? 'X':'O');
            printTabuleiro();
            mudarJogador();
            verificarVitoria();
            jogadaValida = true;
        } else {
            System.out.println("Celula Cheia");
            jogadaValida = false;
        }
        return jogadaValida;
    }

    public static void mudarJogador() {
        nJogadas++;
        if ((nJogadas % 2) == 0) {
            jogador = true;
            System.out.println("Jogador 2 finalizou a jogada");
        } else {
            jogador = false;
            System.out.println("Jogador 1 finalizou a jogada");
        }
        // (((nJogadas % 2) == 0) ? jogador = true : jogador = false);
    }

    public static boolean verificarVitoria() {
        boolean vitoriaValida = false;
        if ((tabuleiro[0][0] == 'X' && tabuleiro[0][1] == 'X' && tabuleiro[0][2] == 'X') || (tabuleiro[1][0] == 'X' && tabuleiro[1][1] == 'X' && tabuleiro[1][2] == 'X')|| (tabuleiro[2][0] == 'X' && tabuleiro[2][1] == 'X' && tabuleiro[2][2] == 'X')|| (tabuleiro[0][0] == 'X' && tabuleiro[1][0] == 'X' && tabuleiro[2][0] == 'X')|| (tabuleiro[0][1] == 'X' && tabuleiro[1][1] == 'X' && tabuleiro[2][1] == 'X')|| (tabuleiro[0][2] == 'X' && tabuleiro[1][2] == 'X' && tabuleiro[2][2] == 'X')|| (tabuleiro[0][0] == 'X' && tabuleiro[1][1] == 'X' && tabuleiro[2][2] == 'X')|| (tabuleiro[0][2] == 'X' && tabuleiro[1][1] == 'X' && tabuleiro[2][0] == 'X')) {
            vitoriaValida = true;
            System.out.println("Vitória do Jogador 1");
        }
        if ((tabuleiro[0][0] == 'O' && tabuleiro[0][1] == 'O' && tabuleiro[0][2] == 'O') || (tabuleiro[1][0] == 'O' && tabuleiro[1][1] == 'O' && tabuleiro[1][2] == 'O')|| (tabuleiro[2][0] == 'O' && tabuleiro[2][1] == 'O' && tabuleiro[2][2] == 'O')|| (tabuleiro[0][0] == 'O' && tabuleiro[1][0] == 'O' && tabuleiro[2][0] == 'O')|| (tabuleiro[0][1] == 'O' && tabuleiro[1][1] == 'O' && tabuleiro[2][1] == 'O')|| (tabuleiro[0][2] == 'O' && tabuleiro[1][2] == 'O' && tabuleiro[2][2] == 'O')|| (tabuleiro[0][0] == 'O' && tabuleiro[1][1] == 'O' && tabuleiro[2][2] == 'O')|| (tabuleiro[0][2] == 'O' && tabuleiro[1][1] == 'O' && tabuleiro[2][0] == 'O')) {
            vitoriaValida = true;
            System.out.println("Vitória do Jogador 2");
        }

        return vitoriaValida;
    }
}
