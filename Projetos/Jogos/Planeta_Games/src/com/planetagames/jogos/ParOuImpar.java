package com.planetagames.jogos;

import java.util.Scanner;

public class ParOuImpar {
    public static Scanner receber = new Scanner(System.in);

    public static void inicio(){
        parOuImparInformacaoJogo();
        parOuImparJogo();
    }

    public static void parOuImparInformacaoJogo(){
        System.out.println("-----------------------");
        System.out.println("----- Par ou Impar ----");
        System.out.println("-----------------------\n");
        System.out.println("###################################\n\n");
        System.out.println("Escolhe Par ou Impar");
        System.out.println("Depois tu e o jogador vão escolher um número cada");
        System.out.println("A soma dos dois irá resultar um número");
        System.out.println("Se se for o tipo de número que escolheste, venceste\n");
    }
    public static void parOuImparJogo(){
        System.out.println("Escolhe uma das opções:");
        System.out.println("1 -> Par");
        System.out.println("2 -> Impar");
        System.out.print("Opção: ");
        int numeroEscolhido = receber.nextInt();
        comercarParOuImpar(numeroEscolhido);
    }

    public static String comercarParOuImpar(int numeroEscolhido){
        return null;
    }
}
