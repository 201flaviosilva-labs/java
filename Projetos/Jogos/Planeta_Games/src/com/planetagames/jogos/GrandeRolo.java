package com.planetagames.jogos;

import com.planetagames.recursos.*;

import java.util.Scanner;//Impotação da biblioteca do scanner

public class GrandeRolo {

    public static Scanner receber = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador
    public static int somaDadosUtilizador;
    public static int somaDadosCpu;

    public static void inicio() {
        grandeRoloInformacaoJogo();
        grandeRoloMethod();
    }

    public static void grandeRoloInformacaoJogo() {
        System.out.println("-------------------");
        System.out.println("--- Grande Rolo ---");
        System.out.println("-------------------");
        System.out.println("");
        System.out.println("###################################");
        System.out.println("");
        System.out.println("");
        System.out.println("Neste jogo tu e o computador vão laçar 4 dados");
        System.out.println("Todos os dados são numerados de 1 - 6");
        System.out.println("O que tiver maior pontução vençe");
        System.out.println("");
    }// end grandeRoloInformacaoMethod

    public static void grandeRoloMethod() {
        //Zerar variaveis
        somaDadosUtilizador = 0;
        somaDadosCpu = 0;

        for (int count1 = 1; count1 <= 4; count1++) { // manda rodar os 4 dados

            ClassPausa.pausaMethod(2000);

            for (int count2 = 1; count2 <= 2; count2++) {
                // count2 == 1 -> Jogador;
                // count2 == 2 -> Computador;

                if (count2 == 1) {
                    System.out.println("");
                    System.out.println("--------- Dados Utilizador! ----------");
                    System.out.println("");
                } // end if
                else {
                    System.out.println("");
                    System.out.println("--------- Dados Computador! ----------");
                    System.out.println("");
                } // end else

                int randomDado = dadosRandomMethod(); //chama o method e faz a seleção do dado

                System.out.println("");
                System.out.println("No " + count1 + " dado calhou: " + randomDado);
                if (count2 == 1) {
                    somaDadosUtilizador += randomDado;
                    System.out.println("A soma até agora é de: " + somaDadosUtilizador); //pontuação minima -> 4; Pontuação máxima -> 24 = 4 dados * 6 máximo número;
                } // end if
                else {
                    somaDadosCpu += randomDado;
                    System.out.println("A soma até agora é de: " + somaDadosCpu);
                } // end else
                System.out.println("");
                ClassPausa.pausaMethod(1500);
            }//end for
        }//end for
        decisaoVitoriaDerrotaMethod();
    }//end method Grande rolo

    public static Integer dadosRandomMethod() {
        int randomDado = (int) (Math.random() * 6) + 1;
        //Desenha o Dado
        if (randomDado == 6) {
            System.out.println("---------\n| *   * |\n| *   * |\n| *   * |\n---------");
        }//end if
        else if (randomDado == 5) {
            System.out.println("---------\n| *   * |\n|   *   |\n| *   * |\n---------");
        }//end else if
        else if (randomDado == 4) {
            System.out.println("---------\n| *   * |\n|       |\n| *   * |\n---------");
        }//end else if
        else if (randomDado == 3) {
            System.out.println("---------\n|     * |\n|   *   |\n| *     |\n---------");
        }//end else if
        else if (randomDado == 2) {
            System.out.println("---------\n|     * |\n|       |\n| *     |\n---------");
        }//end else if
        else {
            System.out.println("---------\n|       |\n|   *   |\n|       |\n---------");
        }//end else
        return randomDado;
    }//end method iniciarMethod

    public static void decisaoVitoriaDerrotaMethod() { //Perceber quem tem a maior pontuação

        System.out.println("Pontuação Utilizador -> " + somaDadosUtilizador);
        System.out.println("Pontuação Computador -> " + somaDadosCpu);
        System.out.println("");
        if (somaDadosCpu < somaDadosUtilizador) {
            System.out.println("Venceste!");
            System.out.println("");
            System.out.println("###################################");
            System.out.println("");
        }//end if
        else if (somaDadosCpu > somaDadosUtilizador) {
            System.out.println("Perdeste!");
            System.out.println("");
            System.out.println("###################################");
            System.out.println("");
        }//end else if
        else {
            System.out.println("O jogo ficou empatado!");
            System.out.println("");
        }//end else
    }//end method decisaoVitoriaDerrotaMethod
}