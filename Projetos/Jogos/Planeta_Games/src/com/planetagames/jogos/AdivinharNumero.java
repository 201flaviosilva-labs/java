package com.planetagames.jogos;

import java.util.Scanner;//Impotação da biblioteca do scanner

public class AdivinharNumero{

	public static Scanner receber = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador

	public static void inicio(){
		adivinhaNumeroInformacaoJogo();
		adivinhaNumeroJogo();
	}

	public static void adivinhaNumeroInformacaoJogo(){
		System.out.println("-----------------------");
		System.out.println("-- Adivinha o Número --");
		System.out.println("-----------------------");
		System.out.println("");
		System.out.println("###################################");
		System.out.println("");
		System.out.println("");
		System.out.println("Um número foi escondido de 1 - 100");
		System.out.println("Tem 5 tentativas de acertar no número");
		//System.out.println("Quanto mais rápido acertares mais ganhas: \n 1 -> aposta * 10 \n 2 -> aposta * 7 \n 3 -> aposta * 5 \n 4 -> aposta * 3 \n 5 -> aposta * 2");
		System.out.println("");
	}//end adivinhaNumeroInformacaoJogo

	public static void adivinhaNumeroJogo(){
		int somaNumeroOportunidades = 0;
		int aleatorio = (int)(Math.random() * 100) + 1;

		for (int count1 = 0; count1 < 5; count1++ ) {
			System.out.print("Diz um número: ");
			int numeroApostado = receber.nextInt();
			System.out.println("");
			System.out.println("------------------------------");
			System.out.println("");

			if (numeroApostado < aleatorio) {
				System.out.println("tentativa pequena!");
			} else if (numeroApostado > aleatorio) {
				System.out.println("TENTATIVA ALTA!");
			}else if (numeroApostado == aleatorio) {
				System.out.println("Acertaste no número!");
				break;
			}else{
				System.out.println("Error!");
			}//end else

			System.out.println("");
			somaNumeroOportunidades++;
			System.out.println("Já gastate " + somaNumeroOportunidades + "/5 oportunidades");
			System.out.println("");
			System.out.println("------------------------------");
			System.out.println("");
		}//end For

		System.out.println("");
		System.out.println("Utilizas-te: " + somaNumeroOportunidades + " oportunidades");
		System.out.println("O número escondido é: " + aleatorio);
		System.out.println("");
		System.out.println("------------------------------");
		System.out.println("");
	} // end adivinhaNumeroJogo
}//end class