package com.planetagames.shiprace;

public class Ship {
    int velocidade;
    int pontos;

    public Ship(int velocidade) {
        this.velocidade = velocidade;
    }

    @Override
    public String toString() {
        return " Velocidade " + velocidade + " máxima," + " Pontos: " + pontos;
    }
}
