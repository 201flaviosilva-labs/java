package com.planetagames.shiprace;

import java.util.LinkedList;

public class RaceShip {
    public static void main(String[] args) {
        inicio();
    }

    public static void inicio() {

        LinkedList<Ship> nave = new LinkedList<>();

        nave.add(new Ship(3000));
        nave.add(new Ship(5000));
        nave.add(new Ship(2000));
        nave.add(new Ship(2500));
        nave.add(new Ship(3500));
        nave.add(new Ship(5500));
        nave.add(new Ship(4500));
        nave.add(new Ship(4000));
        nave.add(new Ship(1500));

        System.out.println("Naves: ");
        for (int i = 0; i < nave.size(); i++) {
            System.out.println("Numº: " + i + nave.get(i).toString());
        }

        System.out.println();
        System.out.println("Corrida vai começar");
        for (int i = 0; i < 10; i++) {
            System.out.println("Volta: " + i);
            for (int j = 0; j < nave.size(); j++) {
                int randomVelu = nave.get(j).pontos + (int) (Math.random() * nave.get(j).velocidade);
                Ship s = nave.get(j);
                s.pontos = randomVelu;
            }
            System.out.println();
        }
        System.out.println("Corrida acabou");
    }
}
