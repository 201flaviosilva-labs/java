package com.planetagames.recursos;

import com.planetagames.carrace.CarRace;
import com.planetagames.jogodogalo.JogoDoGalo;
import com.planetagames.jogos.*;
import com.planetagames.shipcombat.ShipCombat;
import com.planetagames.shiprace.RaceShip;

import java.util.Scanner;//Impotação da biblioteca do scanner

public class Planeta_Games {

    public static Scanner receber = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador
    public static int escolhaMenuIncial;
    public static int jogoEscolhido = 1;

    public static void main(String[] args) { // method que começa o programa
        LimparEcra.clearScreen();
        init(); //Chama o method inicial
    }//end method main

    public static void init() {
        System.out.println("-------------------");
        System.out.println("-- Planeta Games --");
        System.out.println("-------------------");
        System.out.println("");
        System.out.println("###################################");
        System.out.println("");
        menuInicialMethod();
    }//end method iniciarMethod

    public static void menuInicialMethod() {
        while (jogoEscolhido != 0) { //Enquanto o utilizador não escolher 0 o jogo está sempre a perguntar qual jogo quer jogar
            System.out.println();
            System.out.println("Selecciona o número do jogo que queres jogar.");
            System.out.println("");
            System.out.println("1 -> Grande Rolo");
            System.out.println("2 -> Adivinhar Número");
            System.out.println("3 -> KeyBoard Key");
            System.out.println("4 -> Adivinhar Carta");
            System.out.println("5 -> Ship Combat");
            System.out.println("6 -> Car Race");
            System.out.println("7 -> Par ou Impar");
            System.out.println("8 -> Space Invaders");
            System.out.println("9 -> Corrida de Cavalos");
            System.out.println("10 -> Corrida de Naves");
            System.out.println("11 -> Jogo do Galo");
            System.out.println("0 -> Sair");
            System.out.println("");
            System.out.print("Resposta: ");
            jogoEscolhido = receber.nextInt();
            System.out.println("");
            jogoEscolhidoMethod();
        }//end while
    }// end menuInicialMethod

    public static void jogoEscolhidoMethod() {
        LimparEcra.clearScreen();
        //Informar da decisão
        if (jogoEscolhido == 1) {
            GrandeRolo.inicio();
        } else if (jogoEscolhido == 2) {
            AdivinharNumero.inicio();
        } else if (jogoEscolhido == 3) {
            KeyBoardKey.inicio();
        } else if (jogoEscolhido == 4) {
            AdivinhaCarta.inicio();
        } else if (jogoEscolhido == 5) {
            ShipCombat.inicio();
        } else if (jogoEscolhido == 6) {
            CarRace.inicio();
        } else if (jogoEscolhido == 7) {
            ParOuImpar.inicio();
        } else if (jogoEscolhido == 8) {
            System.out.println("Não funcional, necessario atualização ou inicializar o jogo a partir do código");
        } else if (jogoEscolhido == 9) {
            System.out.println("Não funcional");
            //Race.inicio();
        } else if (jogoEscolhido == 10) {
            RaceShip.inicio();
        } else if (jogoEscolhido == 11) {
            JogoDoGalo.inicio();
        } else {
            System.out.println("Até mais ver");
        }//end else
    }//end method voltarAJogarDecisaoMethod
} //end class
