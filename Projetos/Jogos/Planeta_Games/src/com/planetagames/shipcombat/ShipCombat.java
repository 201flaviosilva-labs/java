package com.planetagames.shipcombat;

import java.util.Scanner;

public class ShipCombat {

    public static int count = 5;

    public static void inicio() {
        Scanner receber = new Scanner(System.in); //Variavel de imput

        Ship[] ship = new Ship[count];

        ship[1] = new Ship(100, 5, 5);
        ship[2] = new Ship(50, 9, 10);
        ship[3] = new Ship(70, 2, 2);
        ship[4] = new Ship(25, 1, 4);

        outputShips(ship);
        int[] conjunto = new int[3];
        seleccionarNave(ship, conjunto);

        /*
         System.out.println("");
        System.out.println("Quem vai vencer no combate 1 -> ");
        System.out.print("Número da Nave: ");
        int vencedor1 = receber.nextInt();
        */

        Ship combateShip1 = null;
        Ship combateShip2 = null;

        if (conjunto[1] == 1) {
            combateShip1 = new Ship(ship[1].vida, ship[1].velocidade, ship[1].ataque);
        } else if (conjunto[1] == 2) {
            combateShip1 = new Ship(ship[2].vida, ship[2].velocidade, ship[2].ataque);
        } else if (conjunto[1] == 3) {
            combateShip1 = new Ship(ship[3].vida, ship[3].velocidade, ship[3].ataque);
        } else if (conjunto[1] == 4) {
            combateShip1 = new Ship(ship[4].vida, ship[4].velocidade, ship[4].ataque);
        }

        if (conjunto[2] == 1) {
            combateShip2 = new Ship(ship[1].vida, ship[1].velocidade, ship[1].ataque);
        } else if (conjunto[2] == 2) {
            combateShip2 = new Ship(ship[2].vida, ship[2].velocidade, ship[2].ataque);
        } else if (conjunto[2] == 3) {
            combateShip2 = new Ship(ship[3].vida, ship[3].velocidade, ship[3].ataque);
        } else if (conjunto[2] == 4) {
            combateShip2 = new Ship(ship[4].vida, ship[4].velocidade, ship[4].ataque);
        }

        int numRepeticoes = 0;
        System.out.println("Batalha");
        do {
            System.out.println("-------------------------");
            System.out.println("Ronda: " + ++numRepeticoes);
            assert combateShip1 != null;
            if ((numRepeticoes % (combateShip1.velocidade)) == 0) {
                assert combateShip2 != null;
                combateShip2.vida -= combateShip1.ataque;
                System.out.println("Nave " + conjunto[1] + " Atacou " + combateShip1.ataque + " a Nave " + conjunto[2]);
            }
            assert combateShip2 != null;
            if ((numRepeticoes % (combateShip2.velocidade)) == 0) {
                combateShip1.vida -= combateShip2.ataque;
                System.out.println("Nave " + conjunto[2] + " Atacou " + combateShip2.ataque + " a Nave " + conjunto[1]);
            }

            System.out.println("\nVida Nave " + conjunto[1] + " -> " + combateShip1.vida);
            System.out.println("Vida Nave " + conjunto[2] + " -> " + combateShip2.vida + "\n");

        } while (!(combateShip1.vida > 0) | (combateShip2.vida > 0));
    }

    public static void outputShips(Ship[] ship) {
        System.out.println("---- Ship Combat ---\n");
        System.out.println("Nave 1\nVida -> " + ship[1].vida + "\nVelocidade -> " + ship[1].velocidade + "\nAtaque -> " + ship[1].ataque + "\n");
        System.out.println("Nave 2\nVida -> " + ship[2].vida + "\nVelocidade -> " + ship[2].velocidade + "\nAtaque -> " + ship[2].ataque + "\n");
        System.out.println("Nave 3\nVida -> " + ship[3].vida + "\nVelocidade -> " + ship[3].velocidade + "\nAtaque -> " + ship[3].ataque + "\n");
        System.out.println("Nave 4\nVida -> " + ship[4].vida + "\nVelocidade -> " + ship[4].velocidade + "\nAtaque -> " + ship[4].ataque + "\n");
    }

    public static void seleccionarNave(Ship[] ship, int[] conjunto) {
        int numShip = 0;
        do {
            int randomShip = (int) (Math.random() * count) + 1;

            if (randomShip == 1 && !ship[1].escolhido) {
                ship[1].escolhido = true;
                numShip++;
                conjunto[numShip] = randomShip;
            } else if (randomShip == 2 && !ship[2].escolhido) {
                ship[2].escolhido = true;
                numShip++;
                conjunto[numShip] = randomShip;
            } else if (randomShip == 3 && !ship[3].escolhido) {
                ship[3].escolhido = true;
                numShip++;
                conjunto[numShip] = randomShip;
            } else if (randomShip == 4 && !ship[4].escolhido) {
                ship[4].escolhido = true;
                numShip++;
                conjunto[numShip] = randomShip;
            }
        } while (numShip != 2);

        System.out.println("\nAdversários: \nNave " + conjunto[1] + " VS Nave " + conjunto[2] + "\n");

    }
}
