package com.planetagames.carrace;

public class Cars {
    String marca;
    String modelo;
    int capacidadeMotor; //-> 0 - 100
    int ano; // -> 0 - 100
    int aerodinamica; //-> 0 - 100
    int peso; // -> 0 - 100
    int nitro; // -> 0 - 100
    int deposito; // -> 0 - 100
    int oleo; // -> 0 - 100
    int aderencia; // -> 0 - 100
    int rotacao; // -> 0 - 100

    private double velocidadeMaxima; // soma de tudo
    private double velocidadeMedia; // velocidadeMaxima / 2

    public Cars(String marca, String modelo, int capacidadeMotor, int ano, int aerodinamica, int peso, int nitro, int deposito, int oleo, int aderencia, int rotacao) {
        this.marca = marca;
        this.modelo = modelo;
        this.capacidadeMotor = capacidadeMotor;
        this.ano = ano;
        this.aerodinamica = aerodinamica;
        this.peso = peso;
        this.nitro = nitro;
        this.deposito = deposito;
        this.oleo = oleo;
        this.aderencia = aderencia;
        this.rotacao = rotacao;

        this.velocidadeMaxima = (capacidadeMotor + ano + aerodinamica + peso + nitro + deposito + oleo + aderencia + rotacao);
        this.velocidadeMedia = this.velocidadeMaxima / 2;
    }

    public double getVelocidadeMaxima() {
        return this.velocidadeMaxima;
    }

    public double getVelocidadeMedia() {
        return this.velocidadeMedia;
    }

}
