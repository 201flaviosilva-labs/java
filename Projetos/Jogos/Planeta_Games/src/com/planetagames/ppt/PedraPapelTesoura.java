package com.planetagames.ppt;

import java.util.Scanner;

public class PedraPapelTesoura {
    public static int vitoriasJogador = 0;
    public static int vitoriasComputador = 0;

    public static void main(String[] args) {
        inicio();
    }

    public static void inicio() {
        Scanner keyboard = new Scanner(System.in);
        int escolhaJogador;
        System.out.println("Pedra Papel Tesoura");
        do {
            System.out.println("Vitórias Jogador: " + vitoriasJogador + " - Computador: " + vitoriasComputador);
            System.out.println("Planeia a jogada: ");
            System.out.println("1 -> Pedra");
            System.out.println("2 -> Papel");
            System.out.println("3 -> Tesoura");
            System.out.println("0 -> Sair");
            System.out.print("Número: ");
            escolhaJogador = keyboard.nextInt();
            System.out.println();

            if (escolhaJogador < 0 || escolhaJogador > 3) {
                System.out.println("Error -> Número Inválido");
                continue;
            }

            batalha(escolhaJogador);
        } while (escolhaJogador != 0);

        System.out.println("Até Mais Ver :(");
        System.exit(0);
    }

    public static void batalha(int escolhaJogador) {
        switch (escolhaJogador) {
            case 1:
                System.out.print("Jogador: Pedra");
                break;
            case 2:
                System.out.print("Jogador: Papel");
                break;
            case 3:
                System.out.print("Jogador: Tesoura");
                break;
        }
        System.out.print(" VS ");
        int randomCpu = (int) Math.random() * 3 + 1;
        switch (randomCpu) {
            case 1:
                System.out.println("Computador: Pedra");
                break;
            case 2:
                System.out.println("Computador: Papel");
                break;
            case 3:
                System.out.println("Computador: Tesoura");
                break;
        }

        // 1 -> Pedra
        // 2 -> Papel
        // 3 -> Tesoura

        // Pedra -> Tesoura
        // Papel -> pedra
        // Tesoura -> Papel
        if (escolhaJogador == randomCpu) {
            System.out.println("Empate!");
        } else if ((escolhaJogador == 1 && randomCpu == 3) || (escolhaJogador == 2 && randomCpu == 1) || (escolhaJogador == 3 && randomCpu == 2)) {
            System.out.println("--------- Venceste! ---------");
            vitoriasJogador++;
        } else {
            System.out.println("!!! Perdeste !!!");
            vitoriasComputador++;
        }
        System.out.println();
    }
}
