package com.planetagames.HorseRacs;

public class Race {
    public static void inicio() {
        System.out.println("----------------------");
        System.out.println("----- Horse Race -----");
        System.out.println("----------------------");
        System.out.println();

        Horse[] horse = new Horse[5];
        for (int i = 0; i < horse.length; i++) {
            horse[i] = new Horse();
        }

        System.out.println("Faz a tua aposta no cavalo entre 1 a 5");

        insertUser(horse);

        System.out.println("Cavalo 0 -> " + horse[0].getnApostas());
        System.out.println("Cavalo 1 -> " + horse[1].getnApostas());
        System.out.println("Cavalo 2 -> " + horse[2].getnApostas());
        System.out.println("Cavalo 3 -> " + horse[3].getnApostas());
        System.out.println("Cavalo 4 -> " + horse[4].getnApostas());

        int totalApostas = Horse.nTotalApostas;

        int rand = (int) (Math.random() * totalApostas) + 1;

        System.out.println("Total numero das apostas: " + totalApostas);
        System.out.println("Cavalo vencedor tem pontuação -> " + rand);

        int pontuacaoAtual = 0;
        for (int i = 0; i < horse.length; i++) {
            pontuacaoAtual = pontuacaoAtual + horse[i].getnApostas();
            if (rand < pontuacaoAtual + horse[i].getnApostas()) {
                System.out.println("O cavalo Vencedor é : " + i);
                break;
            }
        }
    }

    public static Horse[] insertUser(Horse[] horse) {
        horse[0].setnApostas();
        horse[0].setnApostas();

        horse[1].setnApostas();
        horse[1].setnApostas();
        horse[1].setnApostas();
        horse[1].setnApostas();
        horse[1].setnApostas();

        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();
        horse[2].setnApostas();

        horse[4].setnApostas();
        horse[4].setnApostas();
        return horse;
    }
}
