import java.util.Scanner;
public class KeyBoardKey{
	public static void main(String[] args) {
		// Zona variaveis
		Scanner receber = new Scanner (System.in); //Variavel de imput
		char resposta; // Variavel que recebe a resposta do utilizador
		String jogarDeNovo = "Sim"; //Predefine como o jogador vai querer jogar de novo no Ciclo While

		char [] letras = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N','O', 'P', 'Q', 'R', 'S', 'T', 'U','V','W', 'X', 'Y', 'Z'};
		char [] numeros = {'0','1','2','3','4','5','6','7','8','9'};
		char [] simbolos = {'§', '±', '!', '¡', '"', 'ﬁ', '@', '#', '€', 'ﬂ', '$', '¢', '%', '∞','&', '•', '/', '(', '{', '[', ')','}',']', '=', '≠', '≈', '?', '¿', '+', '*', '◊', '´', '`', '˝', '¨', '\'', '\\', '|', '‹', '›','º','ª','~','^','˚','ˆ','-','_','.','.',':','…','·',',',';','<','>','≤','≥'};
		
		char carcterEscolhidoAleatorio; // Recebe o caracter que foi escolhido
		int caracterAleatorio; //Variavel aleatória que escolhe qual é o tipo de caracter e detro do tipo qual é o caracter
		int nJogadas = 0, nCertos = 0, nErrados = 0; //criação de variaveis contadoras

		System.out.println("Para sair escrever 'Sair'!"); // Informa o utilizador como sair do jogo
		System.out.println(""); // Criar espaço

		while (jogarDeNovo.equals("Sim")){ // Inicia o Ciclo
			caracterAleatorio = (int) (Math.random() * 4) + 1; // cria um número aletório
			nJogadas++; //Soma o Número de Jogadas

			if (caracterAleatorio == 1) { // letras Maiusculas
				caracterAleatorio = (int) (Math.random() * letras.length); // cria um número aletório
				carcterEscolhidoAleatorio = letras[caracterAleatorio]; // Recebe o carcter da categoria letras Maiusculas
				System.out.println("Escerve: " + carcterEscolhidoAleatorio); // diz qual é o caracter que o utilizador tem que escrver
				System.out.println(""); // Cria um espaço
			}//end if
			else if (caracterAleatorio == 2) { // letras minusculas
				caracterAleatorio = (int) (Math.random() * letras.length); // cria um número aletório
				carcterEscolhidoAleatorio = Character.toLowerCase(letras[caracterAleatorio]); // Recebe o carcter da categoria letras minusculas
				System.out.println("Escerve: " + carcterEscolhidoAleatorio); // diz qual é o caracter que o utilizador tem que escrver
				System.out.println(""); // Cria um espaço
			}//end else if
			else if (caracterAleatorio == 3) { // números
				caracterAleatorio = (int) (Math.random() * numeros.length); // cria um número aletório
				carcterEscolhidoAleatorio = numeros[caracterAleatorio]; // Recebe o carcter da categoria números
				System.out.println("Escerve: " + carcterEscolhidoAleatorio); // diz qual é o caracter que o utilizador tem que escrver
				System.out.println(""); // Cria um espaço
			}//end else if
			else{ // simbolos
				caracterAleatorio = (int) (Math.random() * simbolos.length); // cria um número aletório
				carcterEscolhidoAleatorio = simbolos[caracterAleatorio]; // Recebe o carcter da categoria simbolos
				System.out.println("Caracter: " + carcterEscolhidoAleatorio); //diz qual é o caracter que o utilizador tem que escrver
				System.out.println(""); // Cria um espaço
			}// end else

			System.out.print("Escreve: "); //esta mensagem vai aparcer antes do imput
			jogarDeNovo = receber.next(); // Manda o valor recebido para uma variavel tipo String
			resposta = jogarDeNovo.charAt(0); // Recebe o primero caracter da String e encrementa na char

			//Retificar que o utilizador acertou na resposta
			if (resposta == carcterEscolhidoAleatorio) { // acertou
				System.out.println("Certo"); //Diz que acertou
				nCertos++; //Aumenta os Certos
				System.out.println(""); //Cria um espaço
			}//end if
			else{ //Se não
				System.out.println("Errado"); // Errou
				nErrados++; // Aumenta os Errados
				System.out.println(""); //Cria um espaço
			}// end else

			System.out.println("Nº Certas -> " + nCertos + " Nº Errados -> " + nErrados + " em: " + nJogadas);
			System.out.println("");

			//Confirma se O jogador quer sair ou não
			if (jogarDeNovo.equals("Sair") | jogarDeNovo.equals("sair")) { //se o jogador escrever Sair
				jogarDeNovo = "Sair"; // Diz que o jogador quer sair do jogo
				System.out.println("Bye!"); //Despede do Jogador
			} //end if
			else{ // se não
				jogarDeNovo = "Sim"; // Claro que sim
			}// end else
		}//end while
	} // end method
} // end class
