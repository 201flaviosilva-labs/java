import java.util.Scanner;//Impotação da biblioteca do scanner
public class GrandeRolo_Evolucao{

		//variaveis Geral
		public static Scanner receber = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador
		public static String nomeUtilizador = "";
		public static int pausa = 1500; //pausa o laçamento dos dados durante 1 segundo e meio



		//Variaveis Grande Rolo
		public static int somaDadosUtilizador;
		public static int somaDadosCpu;
		public static char respostaUtilizador = 'S';


	public static void main(String[] args) {
		iniciarMethod(); //Chama o method inicial
		grandeRoloMethod(); //chama o method do jogo Grande rolo
	}//end method main


	public static void iniciarMethod(){
		System.out.println("-----------------");
		System.out.println("-- Grande Rolo --");
		System.out.println("-----------------");
		System.out.println("Neste jogo o teu objetivo:\n -> Rolar 4 dados\n -> Ter mais pontos que o adversário");
		System.out.println("");
		System.out.print("Qual é o teu nome? ");
		System.out.print("Nome: ");
		nomeUtilizador = receber.nextLine();
		System.out.println("");
		System.out.println("###################################");
		System.out.println("");
	}//end method iniciarMethod


	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------< Grande Rolo >---------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public static void grandeRoloMethod(){

		while (respostaUtilizador == 'S') { //Enquanto o utilizador disser "S" o jogo recomeça

			//Zerar variaveis
			somaDadosUtilizador = 0;
			somaDadosCpu = 0;

			System.out.println("Os teus dados vão rolar! ");
			System.out.println("-------------");
			System.out.println("----------- Dados do: " + nomeUtilizador + "! ------------");
			System.out.println("");

			//------------------------------------------------------------------

			//Dados do Jogador
			for (int count1 = 1;count1 <= 4;count1 ++) {

				int randomDado = dadosRandomMethod();

				System.out.println("");
				System.out.println("No " + count1 + " dado calhou: " + randomDado); //Escreve o número no dado e a face calhada
				somaDadosUtilizador += randomDado;
				System.out.println("A soma até agora é de: " + somaDadosUtilizador); //pontuação minima -> 4; Pontuação máxima -> 16;
				System.out.println("");

			pausaMethod(1500);
			}//end for

			System.out.println("");
			System.out.println("--------- Dados do: Computador! ----------");
			System.out.println("");

			pausaMethod(2000);

			//------------------------------------------------------------------

			//Dados do Computador
			for (int count1 = 1; count1 <= 4; count1 ++) {
				
				int randomDado = dadosRandomMethod();
				
				System.out.println("");
				System.out.println("No " + count1 + " dado calhou: " + randomDado);
				somaDadosCpu += randomDado;
				System.out.println("A soma até agora é de: " + somaDadosCpu);
				System.out.println("");

				pausaMethod(1500);

			}//end for

			System.out.println("");

			decisaoVitoriaDerrotaMethod();

			jogardeNovoMethod();

			voltarAJogarDecisaoMethod();	
		}//end While

	}//end method Grande rolo

	public static void pausaMethod(int pausa){
		try { Thread.sleep (pausa); } catch (InterruptedException ex) {} //Coisas novas que não sei o que são, mas pausa o jogo
	}

	public static Integer dadosRandomMethod(){
		int randomDado = (int)(Math.random() * 6) + 1;
		//Desenha o Dado
		if (randomDado == 6) {
			System.out.println("---------\n| *   * |\n| *   * |\n| *   * |\n---------");
		}//end if
		else if (randomDado == 5) {
			System.out.println("---------\n| *   * |\n|   *   |\n| *   * |\n---------");
		}//end else if
		else if (randomDado == 4) {
			System.out.println("---------\n| *   * |\n|       |\n| *   * |\n---------");
		}//end else if
		else if (randomDado == 3) {
			System.out.println("---------\n|     * |\n|   *   |\n| *     |\n---------");
		}//end else if
		else if (randomDado == 2) {
			System.out.println("---------\n|     * |\n|       |\n| *     |\n---------");
		}//end else if
		else{
			System.out.println("---------\n|       |\n|   *   |\n|       |\n---------");
		}//end else
	return randomDado;
	}//end method iniciarMethod

	public static void decisaoVitoriaDerrotaMethod(){
		//Perceber quem tem a maior pontuação
			if (somaDadosCpu < somaDadosUtilizador) {
				System.out.println("Venceste!");
			}//end if
			else if (somaDadosCpu > somaDadosUtilizador) {
				System.out.println("Perdeste!");
			}//end else if
			else  {
				System.out.println("O jogo ficou empatado!");
			}//end else
	}//end method decisaoVitoriaDerrotaMethod

	public static void jogardeNovoMethod(){
		Scanner receber = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador
		System.out.println("");
		System.out.print("Queres jogar de novo? ");
		System.out.print("Resposta: ");
		String jogarDeNovo = receber.nextLine();
		respostaUtilizador = jogarDeNovo.charAt(0);
		respostaUtilizador = Character.toUpperCase(respostaUtilizador); //transforma a letra em MAIUSCULA
		System.out.println("");
	}//end method jogardeNovoMethod

	public static void voltarAJogarDecisaoMethod(){
		//Informar da decisão
		if (respostaUtilizador == 'S') {
			System.out.println("Escolheste jogar de novo!");
			System.out.println("");
			System.out.println("###################################");
		}//end if
		else {
			System.out.println("Até mais ver");
		}//end else
	}//end method voltarAJogarDecisaoMethod
}//end class
