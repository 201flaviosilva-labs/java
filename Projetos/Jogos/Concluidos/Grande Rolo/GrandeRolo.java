import java.util.Scanner;//Impotação da biblioteca do scanner
public class GrandeRolo{
	public static void main(String[] args) {
		Scanner receber = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador

		//int [] dadoUtilizador= new int [4];
		int somaDadosUtilizador = 0;

		//int [] dadoCpu= new int [4];
		int somaDadosCpu = 0;

		char respostaUtilizador = 'S';

		String nomeUtilizador = "";

		int randomDado = 0;

		int pausa = 1500; //pausa o laçamento dos dados durante 1 segundo e meio

		System.out.println("-----------------");
		System.out.println("-- Grande Rolo --");
		System.out.println("-----------------");
		System.out.println("Neste jogo o teu objetivo:\n -> Rolar 4 dados\n -> Ter mais pontos que o adversário");
		System.out.println("");
		System.out.print("Qual é o teu nome? ");
		System.out.print("Nome: ");
		nomeUtilizador = receber.nextLine();
		System.out.println("");
		System.out.println("###################################");
		System.out.println("");

		while (respostaUtilizador == 'S') { //Enquanto o utilizador disser "S" o jogo recomeça

			//Zerar variaveis
			somaDadosUtilizador = 0;
			somaDadosCpu = 0;

			System.out.println("Os teus dados vão rolar! ");
			System.out.println("-------------");
			System.out.println("----------- Dados do: " + nomeUtilizador + "! ------------");
			System.out.println("");

			//Dados do Jogador
			for (int count1 = 1;count1 <= 4;count1 ++) {
				randomDado = (int)(Math.random() * 6) + 1; //Gera um número random de 1 a 6

				//Desenha o Dado
				if (randomDado == 6) {
					System.out.println("---------\n| *   * |\n| *   * |\n| *   * |\n---------");
				}//end if
				else if (randomDado == 5) {
					System.out.println("---------\n| *   * |\n|   *   |\n| *   * |\n---------");
				}//end else if
				else if (randomDado == 4) {
					System.out.println("---------\n| *   * |\n|       |\n| *   * |\n---------");
				}//end else if
				else if (randomDado == 3) {
					System.out.println("---------\n|     * |\n|   *   |\n| *     |\n---------");
				}//end else if
				else if (randomDado == 2) {
					System.out.println("---------\n|     * |\n|       |\n| *     |\n---------");
				}//end else if
				else{
					System.out.println("---------\n|       |\n|   *   |\n|       |\n---------");
				}//end else

				System.out.println("");
				System.out.println("No " + count1 + " dado calhou: " + randomDado); //Escreve o número no dado e a face calhada
				somaDadosUtilizador += randomDado;
				System.out.println("A soma até agora é de: " + somaDadosUtilizador); //pontuação minima -> 4; Pontuação máxima -> 16;
				System.out.println("");

				try { Thread.sleep (pausa); } catch (InterruptedException ex) {} //Coisas novas que não sei o que são, mas pausa o jogo
			}//end for

			System.out.println("");
			System.out.println("--------- Dados do: Computador! ----------");
			System.out.println("");

			try { Thread.sleep (2000); } catch (InterruptedException ex) {}

			//Dados do Computador
			for (int count1 = 1; count1 <= 4; count1 ++) {
				randomDado = (int)(Math.random() * 6) + 1;

				//Desenha o Dado
				if (randomDado == 6) {
					System.out.println("---------\n| *   * |\n| *   * |\n| *   * |\n---------");
				}//end if
				else if (randomDado == 5) {
					System.out.println("---------\n| *   * |\n|   *   |\n| *   * |\n---------");
				}//end else if
				else if (randomDado == 4) {
					System.out.println("---------\n| *   * |\n|       |\n| *   * |\n---------");
				}//end else if
				else if (randomDado == 3) {
					System.out.println("---------\n|     * |\n|   *   |\n| *     |\n---------");
				}//end else if
				else if (randomDado == 2) {
					System.out.println("---------\n|     * |\n|       |\n| *     |\n---------");
				}//end else if
				else{
					System.out.println("---------\n|       |\n|   *   |\n|       |\n---------");
				}//end else
				System.out.println("");
				System.out.println("No " + count1 + " dado calhou: " + randomDado);
				somaDadosCpu += randomDado;
				System.out.println("A soma até agora é de: " + somaDadosCpu);
				System.out.println("");

				try { Thread.sleep (pausa); } catch (InterruptedException ex) {} //Coisas novas que não sei o que são, mas pausa o jogo
			}//end for

			System.out.println("");

			//Perceber quem tem a maior pontuação
			if (somaDadosCpu < somaDadosUtilizador) {
				System.out.println("Venceste!");
			}//end if
			else if (somaDadosCpu > somaDadosUtilizador) {
				System.out.println("Perdeste!");
			}//end else if
			else  {
				System.out.println("O jogo ficou empatado!");
			}//end else

			System.out.println("");
			System.out.print("Queres jogar de novo? ");
			System.out.print("Resposta: ");
			String jogarDeNovo = receber.nextLine();
			respostaUtilizador = jogarDeNovo.charAt(0);
			respostaUtilizador = Character.toUpperCase(respostaUtilizador); //transforma a letra em MAIUSCULA
			System.out.println("");

			//Informar da decisão
			if (respostaUtilizador == 'S') {
				System.out.println("Escolheste jogar de novo!");
				System.out.println("");
				System.out.println("###################################");
			}//end if
			else {
				System.out.println("Até mais ver");
			}//end else
		}//end While
	}//end method
}//end class
