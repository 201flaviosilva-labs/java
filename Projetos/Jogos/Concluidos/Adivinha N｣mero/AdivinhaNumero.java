import java.util.Scanner;
public class AdivinhaNumero{
	public static void main(String[] args) {

		Scanner receberInt = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador
		Scanner receberString = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador

		int somaNumeroOportunidades=0;
		char resposta='S';

		System.out.println("");
		System.out.println("------------------------------");
		System.out.println("------Adivinha o Número-------");
		System.out.println("------------------------------");
		
		while (resposta == 'S'){
			int aleatorio = (int)(Math.random()*100)+1;
			System.out.println("");
			System.out.println("Um número foi escondido de 1-100");
			System.out.println("------------------------------");
			System.out.println("");
			System.out.println("Qual é o máximo de oportunades que queres?");	
			System.out.print("Máximo de oportunidades: ");

			int numMaximoOportunidades=receberInt.nextInt();
			
			System.out.println("");
			System.out.println("------------------------------");
			System.out.println("");

			for (int count1=0;count1<numMaximoOportunidades;count1++ ) {
				System.out.print("Diz um número: ");
				int numeroApostado=receberInt.nextInt();
				System.out.println("");
				System.out.println("------------------------------");
				System.out.println("");

				if (numeroApostado<aleatorio) {
					System.out.println("Tentativa Pequena!");
				} else if (numeroApostado>aleatorio) {
					System.out.println("Tentativa Alta!");
				}else if (numeroApostado==aleatorio) {
					System.out.println("Acertaste no número!");
					break;
				}else{
				System.out.println("Error!");
				}//end else

				System.out.println("");
				somaNumeroOportunidades++;
				System.out.println("Já gastate " + somaNumeroOportunidades + " oportunidades de: " + numMaximoOportunidades);
				System.out.println("");
				System.out.println("------------------------------");
			}//end For

			System.out.println("");
			System.out.println("O teu número de oportunidades foi de: "+ somaNumeroOportunidades );
			System.out.println("O número escondido é: " + aleatorio);
			System.out.println("");
			System.out.println("------------------------------");
			System.out.println("");
			System.out.println("Queres jogar de novo?");
			System.out.print("Resposta: ");
			String jogarDeNovo=receberString.nextLine();
			resposta=jogarDeNovo.charAt(0);
			resposta=Character.toUpperCase(resposta); //transforma a letra em MAIUSCULA
			System.out.println("");
			somaNumeroOportunidades=0;

			//Mostrar a escolha do jogador
			if (resposta=='S') {
				System.out.println("Escolheste jogar de novo!");
				System.out.println("");
				System.out.println("------------------------------");
				System.out.println("");
			}//end if
			else {
				System.out.println("Até mais ver!!");
			}//end else
		}//end While
		System.out.println("O Jogo Acabou!");
	}//end method
}//end class
