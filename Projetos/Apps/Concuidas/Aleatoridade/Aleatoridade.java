import java.util.Scanner;
public class Aleatoridade{
	public static void main(String[] args) {
		Scanner receberInt = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador de números
		Scanner receberString = new Scanner(System.in);//variavel que vai receber o que for escrito pelo utilizador de palavras
		String resposta="S";
		System.out.println("<----- Número aleatório ---->");
		System.out.println("");
		
		while (resposta.equals("S") | resposta.equals("s")){
			System.out.print("Máximo aleatório: ");
			int numMaximo = receberInt.nextInt();
			String [] nomeJogador = new String [numMaximo+2];
			System.out.println("");

			for (int count1=1;count1<numMaximo+1;count1++ ) {
				System.out.print("Nome do objeto "+count1+": ");
				nomeJogador[count1] =receberString.nextLine();
				System.out.println(count1 + " - " + nomeJogador[count1]);
				System.out.println("");
			}//end for
			int numeroAleatorio=(int) (Math.random()*numMaximo)+1;
			System.out.println("O escolhido foi: " + nomeJogador[numeroAleatorio]);
			System.out.println("");

			System.out.println("Queres escolher outro número?");
			System.out.print("Resposta (S ou N): ");
			resposta=receberString.nextLine();
			System.out.println("");
		}//end while
	}//end method
}//end class