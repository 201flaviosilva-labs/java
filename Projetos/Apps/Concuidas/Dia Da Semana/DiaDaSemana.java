//Versão 2.1

import java.util.Scanner; // importar a biblioteca Scanner 
//import java.util.Date; //Importa o cuscador da data

public class DiaDaSemana {
    public static void main(String[] args) { 
        Scanner receber = new Scanner(System.in);

        System.out.println("Insere o dia: ");
        //int dia = Integer.parseInt(args[0]);
        int dia = receber.nextInt();

        System.out.println("Insere o mês: ");
        //int mes = Integer.parseInt(args[1]);
        int mes = receber.nextInt();

        System.out.println("Insere o ano: ");
        //int ano = Integer.parseInt(args[2]);
        int ano = receber.nextInt();

        /*
        Date dataAtual = new Date(); // variavel que contem o valor da data atual
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); //Variavel que contem o formato da data
        */
        //-----------------Cena das Contas------------
        int ano0 = ano - (14 - mes) / 12;
        int num1 = ano0 + ano0/4 - ano0/100 + ano0/400;
        int mes0 = mes + 12 * ((14 - mes) / 12) - 2;
        int diaD = (dia + num1 + (31*mes0)/12) % 7;



        //-----------------Mês------------
        String mesTexto;
        if (mes==1) {  
            mesTexto="Janeiro";
        }
        else if (mes==2){
            mesTexto="Fevereiro";
        }
        else if (mes==3){
            mesTexto="Março";
        }
        else if (mes==4){
            mesTexto="Abril";
        }
        else if (mes==5){
            mesTexto="Maio";
        }
        else if (mes==6){
            mesTexto="Junho";
        }
        else if (mes==7){
            mesTexto="Julho";
        }
        else if (mes==8){
            mesTexto="Agosto";
        }
        else if (mes==9){
            mesTexto="Setembro";
        }
        else if (mes==10){
            mesTexto="Outubro";
        }
        else if (mes==11){
            mesTexto="Novembro";
        }
        else if (mes==12){
            mesTexto="Dezembro";
        }
        else{
            mesTexto="Erro";
        }

        //-----------------Signo---------------------
        String signoTexto="";
        if ((mes==3 && dia>=21) | (mes==4 && dia<=20)) {  
            signoTexto ="Carneiro";
        }
        else if ((mes==4 && dia>=21) | (mes==5 && dia<=20)){
            signoTexto="Touro";
        }
        else if ((mes==5 && dia>=21) | (mes==6 && dia<=20)){
            signoTexto="Gémeos";
        }
        else if ((mes==6 && dia>=21) | (mes==7 && dia<=21)){
            signoTexto="Caranguejo";
        }
        else if ((mes==7 && dia>=22) | (mes==8 && dia<=22)){
            signoTexto="Leão";
        }
        else if ((mes==8 && dia>=23) | (mes==9 && dia<=22)){
            signoTexto="Virgem";
        }
        else if ((mes==9 && dia>=23) | (mes==10 && dia<=22)){
            signoTexto="Balança";
        }
        else if ((mes==10 && dia>=23) | (mes==11 && dia<=21)){
            signoTexto="Escorpião";
        }
        else if ((mes==11 && dia>=22) | (mes==12 && dia<=21)){
            signoTexto="Sagitário";
        }
        else if ((mes==12 && dia>=22) | (mes==1 && dia<=20)){
            signoTexto="Capricórnio";
        }
        else if ((mes==1 && dia>=21) | (mes==2 && dia<=19)){
            signoTexto="Aquairio";
        }
        else if ((mes==2 && dia>=20) | (mes==3 && dia<=20)){
            signoTexto="Peixes";
        }
        else{
            signoTexto="Erro";
        }


        //-----------Ano Bixesto----------------
        String anoBixesto = "Não";
        if (ano%4==0) {
            anoBixesto="Sim";
        }else{
            anoBixesto="Não";
        }


        System.out.println("");
        System.out.println("---------------------");
        System.out.println("");

        //Tempo para acabar o ano
        int diasParaAcabarAno;
        if (anoBixesto.equals("Sim")) {
            diasParaAcabarAno=30*mes+dia+1;
        }else{
            diasParaAcabarAno=30*mes+dia;
        }

        // ------------Dia da Semana---------------
        String diaSemana="";
        if (diaD==1) {
            diaSemana="a uma Segunda-feira!";
        }
        else if (diaD==2){
            diaSemana="a uma Terça-feira!";
        }
        else if (diaD==3){
            diaSemana="a uma Quarta-feira!";
        }
        else if (diaD==4){
            diaSemana="a uma Quinta-feira!";
        }
        else if (diaD==5){
            diaSemana="a uma Sexta-feira!";
        }
        else if (diaD==6){
            diaSemana="a um Sabado!";
        }
        else{
            diaSemana="a um Domingo!";
            
        }//End else

        int milenio=(ano/1000)+1;

        //-----------------Resposta--------------------
        System.out.println("Dia: " + dia + " Mês: " + mes + "/" + mesTexto + " Ano: " + ano);
        System.out.println("Este dia chalha "+diaSemana);
        System.out.println("O signo é: " + signoTexto); 
        System.out.println("Ano Bixesto: "+anoBixesto);
        System.out.println("Milénio: "+ milenio);
        System.out.println("Século: "+ (ano/1000)+1);
        //System.out.println("Tempo para o ano acabar: "+diasParaAcabarAno);
        System.out.println("");
        System.out.println("# Fim do Programa #");
        System.out.println("");
    }//end Method
}//End Class
