import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JFrame;

public class PixelCanvas extends Canvas {
    private static final int WIDTH = 1000;
    private static final int HEIGHT = 1000;
    private static final Random random = new Random();

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int i = 0;

        do {
            int y =random.nextInt(1000);
            int x =random.nextInt(1000);
            g.setColor(randomColor());
            g.drawLine(y, x, y, x);
        }while (i > -1);
    }

    private Color randomColor() {
        return new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(WIDTH, HEIGHT);
        frame.add(new PixelCanvas());

        frame.setVisible(true);
    }
}