
public class CodigoBinario {

    public static String conversaoAlfabetoToBinario(String palavra) {
        byte[] bytes = palavra.getBytes();
        StringBuilder solucao = new StringBuilder();
        for (byte b : bytes) {
            int val = b;
            for (int i = 0; i < 8; i++) {
                solucao.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
            solucao.append(' ');
        }
        return solucao.toString();
    }

    public static String conversaoBinariotoAlfabeto(String codigo) {
       /* StringBuilder sb = new StringBuilder();
        char[] chars = codigo.replaceAll("\\s", "").toCharArray();
        int[] mapping = {1, 2, 4, 8, 16, 32, 64, 128};

        for (int j = 0; j < chars.length; j += 8) {
            int idx = 0;
            int sum = 0;
            for (int i = 7; i >= 0; i--) {
                if (chars[i + j] == '1') {
                    sum += mapping[idx];
                }
                idx++;
            }
            System.out.println(sum);//debug
            sb.append(Character.toChars(sum));
        }
        return sb.toString(); */
       return null;
    }
}
