
public class CodigoASCII {

    public static String conversaoAlfabetoToASCII(String palavra) {
        String solucao = "";
        int repeticoes = palavra.length();
        for (int i = 0; i < repeticoes; i++) {
            char letra = palavra.charAt(i);
            int ascii = (int) letra;
            solucao = solucao + "  " + ascii;
        }
        return solucao;
    }

    public static String conversaoAsciitoAlfabeto(String codigo) {
        int numero = Integer.parseInt(codigo);
        char letra = (char) numero;
        String resultado = String.valueOf(letra);
        return resultado;
    }
}
